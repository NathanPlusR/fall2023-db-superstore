package dawson;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Product implements SQLData{
    private int product_id;
    private String name;
    private String category;
    private double price;
    public String typeName;


    public Product()
    {

    }

    public Product(
        String name,
        String category,
        double price)
    {
        this.product_id = -1;
        this.name = name;
        this.category = category;
        this.price = price;
        this.typeName = "PRODUCT_OBJECT";
    }

    public Product(
        int product_id,
        String name,
        String category,
        double price)
    {
        this.product_id = product_id;
        this.name = name;
        this.category = category;
        this.price = price;
        this.typeName = "PRODUCT_OBJECT";
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException
    {
        stream.writeInt(this.product_id);
        stream.writeString(this.name);
        stream.writeString(this.category);
        stream.writeDouble(this.price);
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        this.product_id = stream.readInt();
        this.name = stream.readString();
        this.category = stream.readString();
        this.price = stream.readDouble();
    }

    @Override
    public String getSQLTypeName() throws SQLException
    {
        return this.typeName;
    }

    public void AddToDatabase(Connection conn) throws SQLException, ClassNotFoundException
    {
        Map map = conn.getTypeMap();
        conn.setTypeMap(map);
        map.put(this.getSQLTypeName(), Class.forName("dawson.Product"));
        String sql = "{call product_package.add_product(?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.setObject(1, this);
            stmt.execute();
        }
    }

    public void removeFromDatabase(Connection conn) throws SQLException, ClassNotFoundException
    {
        String sql = "{call product_package.delete_product(?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.setObject(1, this.product_id);
            stmt.execute();
        }
    }

    public static void removeFromDatabase(Connection conn, int product_id) throws SQLException, ClassNotFoundException
    {
        String sql = "{call product_package.delete_product(?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.setObject(1, product_id);
            stmt.execute();
        }
    }

    public void updateProduct(Connection conn) throws SQLException, ClassNotFoundException
    {
        Map map = conn.getTypeMap();
        conn.setTypeMap(map);
        map.put(this.getSQLTypeName(), Class.forName("dawson.Product"));
        String sql = "{call product_package.update_product(?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.setObject(1, this);
            stmt.execute();
        }
    }

    public static List<Product> selectProducts(Connection conn) throws SQLException, ClassNotFoundException
    {
        List<Product> products = new ArrayList<Product>();
        String sql = "{call product_package.display_products(?, ?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.registerOutParameter(2, Types.REF_CURSOR);
            stmt.setString(1, "%");
            stmt.execute();
            ResultSet rs = (ResultSet)stmt.getObject(2);

            while(rs.next()){
                int product_id = rs.getInt(1);
                String name = rs.getString(2);
                String category = rs.getString(3);
                double price = rs.getDouble(4);

                products.add(new Product(product_id, name, category, price));
            }
        }

        return products;
    }

    public static List<Product> selectProducts(Connection conn, String category) throws SQLException, ClassNotFoundException
    {
        List<Product> products = new ArrayList<Product>();
        String sql = "{call product_package.display_products(?, ?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.registerOutParameter(2, Types.REF_CURSOR);
            stmt.setString(1, category);
            stmt.execute();
            ResultSet rs = (ResultSet)stmt.getObject(2);

            while(rs.next()){
                int product_id = rs.getInt(1);
                String name = rs.getString(2);
                category = rs.getString(3);
                double price = rs.getDouble(4);

                products.add(new Product(product_id, name, category, price));
            }
        }

        return products;
    }

    public static int getTotalStock(Connection conn, int product_id) throws SQLException, ClassNotFoundException
    {
        int stock = 0;
        String sql = "{call product_package.display_products_stock(?, ?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.registerOutParameter(2, Types.REF_CURSOR);
            stmt.setInt(1, product_id);
            stmt.execute();
            ResultSet rs = (ResultSet)stmt.getObject(2);

            while(rs.next()){
                stock = rs.getInt(1);
            }
        }

        return stock;
    }

    public int getTotalStock(Connection conn) throws SQLException, ClassNotFoundException
    {
        int stock = 0;
        String sql = "{call product_package.display_products_stock(?, ?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.registerOutParameter(2, Types.REF_CURSOR);
            stmt.setInt(1, this.product_id);
            stmt.execute();
            ResultSet rs = (ResultSet)stmt.getObject(2);

            while(rs.next()){
                stock = rs.getInt(1);
            }
        }

        return stock;
    }

    public int timesProductWasOrdered(Connection conn) throws SQLException, ClassNotFoundException
    {
        int timesOrdered = -1;
        String sql = "{call product_package.count_ordered(?, ?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.registerOutParameter(2, Types.INTEGER);
            stmt.setInt(1, this.product_id);
            stmt.execute();
            timesOrdered = (Integer)stmt.getObject(2);
        }

        return timesOrdered;
    }

    public String getCategory() {
        return category;
    }
    public String getName() {
        return name;
    }
    public double getPrice() {
        return price;
    }
    public int getProduct_id() {
        return product_id;
    }
    
    public void setCategory(String category) {
        this.category = category;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public String toString()
    {
        String productIDFormat = String.format("%2s", this.product_id);
        String nameFormat = String.format("%40s", this.name);
        String categoryFormat = String.format("%15s", this.category);
        String priceFormat = String.format("%10s", this.price);

        return "Id: " + productIDFormat
        + "\t\tName: " + nameFormat
        + "\t\tCategory: " + categoryFormat
        + "\tPrice: " + priceFormat;
    }
}
