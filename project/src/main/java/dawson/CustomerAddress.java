package dawson;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomerAddress {
    private String address;
    private int customer_id;
    
    public CustomerAddress(String address, int customer_id)
    {
        this.address = address;
        this.customer_id = customer_id;
    }

    public void AddToDatabase(Connection conn) throws SQLException, ClassNotFoundException
    {
        String sql = "{call customer_address_package.add_customer_address(?, ?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.setObject(1, this.address);
            stmt.setObject(2, this.customer_id);
            stmt.execute();
        }
    }

    public static List<CustomerAddress> selectAddressesByCustomer (Connection conn, int customer_id) throws SQLException, ClassNotFoundException
    {
        List<CustomerAddress> addresses = new ArrayList<CustomerAddress>();
        String sql = "{call customer_address_package.display_customer_addresses(?, ?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.registerOutParameter(2, Types.REF_CURSOR);
            stmt.setInt(1, customer_id);
            stmt.execute();
            ResultSet rs = (ResultSet)stmt.getObject(2);

            while(rs.next()){
                String address = rs.getString(1);
                customer_id = rs.getInt(2);

                addresses.add(new CustomerAddress(address, customer_id));
            }
        }

        return addresses;
    }

    public int getCustomer_id() {
        return customer_id;
    }
    public String getAddress() {
        return address;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }

    public String toString()
    {
        String customerIDFormat = String.format("%2s", this.customer_id);
        String customerAddressFormat = String.format("%50s", this.address);
        return "Customer: " + customerIDFormat + "\tAddress: " + customerAddressFormat ;
    }
}
