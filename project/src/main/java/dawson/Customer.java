package dawson;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Customer {
    private int customer_id;
    private String fname;
    private String lname;
    
    public Customer(String fname, String lname)
    {
        this.customer_id = -1;
        this.fname = fname;
        this.lname = lname;
    }
    
    private Customer(int customer_id, String fname, String lname)
    {
        this.customer_id = customer_id;
        this.fname = fname;
        this.lname = lname;
    }

    public void AddToDatabase(Connection conn) throws SQLException, ClassNotFoundException
    {
        String sql = "{call customer_package.add_customer(?, ?, ?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.setObject(1, this.fname);
            stmt.setObject(2, this.lname);
            stmt.registerOutParameter(3, Types.INTEGER);
            stmt.execute();

            this.customer_id = (Integer)stmt.getObject(3);
        }
    }

    public static Customer selectCustomerByID(Connection conn, int customer_id) throws SQLException, ClassNotFoundException
    {
        Customer c =  null;
        String sql = "{call customer_package.displayCustomerByID(?, ?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.registerOutParameter(2, Types.REF_CURSOR);
            stmt.setInt(1, customer_id);
            stmt.execute();
            ResultSet rs = (ResultSet)stmt.getObject(2);

            while(rs.next()){
                String fname = rs.getString(2);
                String lname = rs.getString(3);

                c = new Customer(customer_id, fname, lname);
            }
        }

        return c;
    }

    public static List<Customer> selectFlaggedCustomers(Connection conn) throws SQLException, ClassNotFoundException
    {
        List<Customer> customers = new ArrayList<Customer>();
        String sql = "{call customer_package.displayFlaggedCustomers(?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.registerOutParameter(1, Types.REF_CURSOR);
            stmt.execute();
            ResultSet rs = (ResultSet)stmt.getObject(1);

            while(rs.next()){
                int customer_id = rs.getInt(1);
                String fname = rs.getString(2);
                String lname = rs.getString(3);

                customers.add(new Customer(customer_id, fname, lname));
            }
        }

        return customers;
    }

    public static List<Customer> selectCustomersList(Connection conn) throws SQLException, ClassNotFoundException
    {
        List<Customer> customers = new ArrayList<Customer>();
        String sql = "{call customer_package.displayCustomers(?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.registerOutParameter(1, Types.REF_CURSOR);
            stmt.execute();
            ResultSet rs = (ResultSet)stmt.getObject(1);

            while(rs.next()){
                int customer_id = rs.getInt(1);
                String fname = rs.getString(2);
                String lname = rs.getString(3);

                customers.add(new Customer(customer_id, fname, lname));
            }
        }

        return customers;
    }

    public String getFname() {
        return fname;
    }

    public String getLname() {
        return lname;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public String toString()
    {
        String customerIDFormat = String.format("%2s", this.customer_id);
        String customerNameFormat = String.format( "%30s" ,this.fname + " " + this.lname);
        return "Customer id: " + customerIDFormat + "     Full Name: " + customerNameFormat;
    }

    public boolean equals(Object obj) {
        try {
            Customer c = (Customer)obj;
            return this.customer_id == c.customer_id;
        } catch (Exception e) {
            return false;
        }
    }
}
