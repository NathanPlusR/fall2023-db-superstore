package dawson;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

public class Warehouse {
    private int warehouse_id;
    private String name;
    private String address;

    public Warehouse(String name, String address) {
        this.name = name;
        this.address = address;
    }

    private Warehouse(int warehouse_id, String name, String address) {
        this(name, address);
        this.warehouse_id = warehouse_id;
    }

    public int getWarehouse_id() {
        return this.warehouse_id;
    }

    public String getName() {
        return this.name;
    }

    public String getAddress() {
        return this.address;
    }

    public void addWarehouse(Connection conn){
        try{
                String sql = "{call warehouse_package.add_warehouse(?, ?)}";
                CallableStatement statement = conn.prepareCall(sql);
                statement.setString(1, this.name);
                statement.setString(2, this.address);
                statement.execute();
        } catch (Exception e){
                System.out.println("Could not add warehouse or it already exists");
        }
    }

    public static void removeWarehouse(Connection conn, int warehouse_id){
        try{
                String sql = "{call warehouse_package.remove_warehouse(?)}";
                CallableStatement statement = conn.prepareCall(sql);
                statement.setInt(1, warehouse_id);
                statement.execute();
        } catch (Exception e){
                System.out.println("Could not remove warehouse or it never existed");
        }
    }

    public static void updateWarehouseInfo(Connection conn, int warehouse_id, String newName ,String newAddress){
        try{
                String sql = "{call warehouse_package.update_warehouse(?, ?, ?)}";
                CallableStatement statement = conn.prepareCall(sql);
                statement.setInt(1, warehouse_id);
                statement.setString(2, newName);
                statement.setString(3, newAddress);
                statement.execute();
        } catch (Exception e){
                System.out.println("Could not update warehouse or it never existed");
        }
    }

    public static List<Warehouse> getAllWarehouses(Connection conn) throws SQLException{
        List<Warehouse> warehouses = new ArrayList<Warehouse>();
        String sql = "{call warehouse_package.get_all_warehouses(?)}";
        CallableStatement statement = conn.prepareCall(sql);
        statement.registerOutParameter(1, Types.REF_CURSOR);
        statement.execute();
        ResultSet rs = (ResultSet) statement.getObject(1);

        while(rs.next()){
            int warehouse_id = rs.getInt("warehouse_id");
            String name = rs.getString("name");
            String address = rs.getString("address");
            warehouses.add(new Warehouse(warehouse_id, name, address));
        }
        return warehouses;
    }

    public static void addProductStock(Connection conn, int warehouse_id, int product_id, int quantity) throws SQLException{
        String sql = "{call warehouse_stock_package.add_warehouse_stock(?, ?, ?)}";
        CallableStatement statement = conn.prepareCall(sql);
        statement.setInt(1, warehouse_id);
        statement.setInt(2, product_id);
        statement.setInt(3, quantity);
        statement.execute();
    }

    public static void updateProductStock(Connection conn,int warehouse_id, int product_id, int quantityChange) throws SQLException{
        String sql = "{call warehouse_stock_package.update_warehouse_stock(?, ?, ?)}";
        CallableStatement statement = conn.prepareCall(sql);
        statement.setInt(1, warehouse_id);
        statement.setInt(2, product_id);
        statement.setInt(3, quantityChange);
        statement.execute();
    }

    public static void removeProductStock(Connection conn, int warehouse_id, int product_id) throws SQLException{
        String sql = "{call warehouse_stock_package.remove_warehouse_stock(?, ?)}";
        CallableStatement statement = conn.prepareCall(sql);
        statement.setInt(1, warehouse_id);
        statement.setInt(2, product_id);
        statement.execute();    
    }

    public String toString()
    {
        String warehouseIDFormat = String.format("%2s", this.warehouse_id);
        String warehouseNameFormat = String.format("%20s", this.name);
        String warehouseAddressFormat = String.format("%80s", this.address);

        return "Id: " + warehouseIDFormat
        + "\tName: " + warehouseNameFormat
        + "\tAddress: " + warehouseAddressFormat;
    }
}
