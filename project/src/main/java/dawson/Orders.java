package dawson;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Orders implements SQLData{
    private int order_id;
    private Date order_date;
    private int quantity;
    private String email;
    private String address;
    private double price;
    private int store_id;
    private int product_id;
    public String typeName;


    public Orders()
    {

    }

    private Orders(
        int order_id,
        Date order_date,
        int quantity,
        String email,
        String address,
        double price,
        int store_id,
        int product_id)
    {
        this.order_id = order_id;
        this.order_date = order_date;
        this.quantity = quantity;
        this.email = email;
        this.address = address;
        this.price = price;
        this.store_id = store_id;
        this.product_id = product_id;
        this.typeName = "ORDERS_OBJECT";
    }

    public Orders(
        int quantity,
        String email,
        String address,
        double price,
        int store_id,
        int product_id)
    {
        this.order_id = -1;
        this.order_date = null;
        this.quantity = quantity;
        this.email = email;
        this.address = address;
        this.price = price;
        this.store_id = store_id;
        this.product_id = product_id;
        this.typeName = "ORDERS_OBJECT";
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException
    {
        stream.writeInt(this.order_id);
        stream.writeDate(this.order_date);
        stream.writeInt(this.quantity);
        stream.writeString(this.email);
        stream.writeString(this.address);
        stream.writeDouble(this.price);
        stream.writeInt(this.store_id);
        stream.writeInt(this.product_id);
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        this.order_id = stream.readInt();
        this.order_date = stream.readDate();
        this.quantity = stream.readInt();
        this.email = stream.readString();
        this.address = stream.readString();
        this.price = stream.readDouble();
        this.store_id = stream.readInt();
        this.product_id =stream.readInt();
    }

    @Override
    public String getSQLTypeName() throws SQLException
    {
        return this.typeName;
    }

    public void AddToDatabase(Connection conn) throws SQLException, ClassNotFoundException
    {
        Map map = conn.getTypeMap();
        conn.setTypeMap(map);
        map.put(this.getSQLTypeName(), Class.forName("dawson.Orders"));
        String sql = "{call orders_package.add_order(?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
        stmt.setObject(1, this);
        stmt.execute();
        }
    }

    public void removeFromDatabase(Connection conn) throws SQLException, ClassNotFoundException
    {
        String sql = "{call orders_package.remove_order(?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.setObject(1, this.order_id);
            stmt.execute();
        }
    }

    public static void removeFromDatabase(Connection conn, int order_id) throws SQLException, ClassNotFoundException
    {
        String sql = "{call orders_package.remove_order(?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.setObject(1, order_id);
            stmt.execute();
        }
    }

    public static List<Orders> selectOrders(Connection conn) throws SQLException, ClassNotFoundException
    {
        List<Orders> orders = new ArrayList<Orders>();
        String sql = "{call orders_package.display_orders(?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.registerOutParameter(1, Types.REF_CURSOR);
            stmt.execute();
            ResultSet rs = (ResultSet)stmt.getObject(1);

            while(rs.next()){

                int order_id = rs.getInt(1);
                Date order_date = rs.getDate(2);
                int quantity = rs.getInt(3);
                String email = rs.getString(4);
                String address = rs.getString(5);
                double price = rs.getDouble(6);
                int store_id = rs.getInt(7);
                int product_id = rs.getInt(8);

                orders.add(new Orders(order_id, order_date, quantity, email, address, price, store_id, product_id));
            }
        }

        return orders;
    }

    public static List<Orders> selectOrdersByStore(Connection conn, int store_id) throws SQLException, ClassNotFoundException
    {
        
        List<Orders> orders = new ArrayList<Orders>();
        String sql = "{call orders_package.display_orders(?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.registerOutParameter(1, Types.REF_CURSOR);
            stmt.execute();
            ResultSet rs = (ResultSet)stmt.getObject(1);

            while(rs.next()){
                if (store_id == rs.getInt(7))
                {
                    int order_id = rs.getInt(1);
                    Date order_date = rs.getDate(2);
                    int quantity = rs.getInt(3);
                    String email = rs.getString(4);
                    String address = rs.getString(5);
                    double price = rs.getDouble(6);
                    int product_id = rs.getInt(8);

                    orders.add(new Orders(order_id, order_date, quantity, email, address, price, store_id, product_id));
                }
            }
        }

        return orders;
    }

    public String getAddress() {
        return address;
    }
    public String getEmail() {
        return email;
    }
    public Date getOrder_date() {
        return order_date;
    }
    public int getOrder_id() {
        return order_id;
    }
    public int getProduct_id() {
        return product_id;
    }
    public int getQuantity() {
        return quantity;
    }
    public int getStore_id() {
        return store_id;
    }
    public double getPrice() {
        return price;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setOrder_date(Date order_date) {
        this.order_date = order_date;
    }
    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }
    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    public void setStore_id(int store_id) {
        this.store_id = store_id;
    }
    public void setPrice(double price) {
        this.price = price;
    }

    public String toString() {
        String orderIDFormat = String.format("%2s", this.order_id);
        String orderDateFormat = String.format("%10s", this.order_date);
        String orderQuantitiyFormat = String.format("%2s", this.quantity);
        String orderEmailFormat = String.format("%30s", this.email);
        String addressFormat = String.format("%60s", this.address);
        String orderPriceFormat = String.format("%10s", this.price);
        String storeIDFormat = String.format("%2s", this.store_id);
        String productIDFormat = String.format("%2s", this.product_id);


        return "Id: " + orderIDFormat +
        "  Date: " + orderDateFormat + 
        "  Quantity: " + orderQuantitiyFormat +
        "  Email: " + orderEmailFormat +
        "  Address: " + addressFormat +
        "  Price: " + orderPriceFormat +
        "  Store ID: " + storeIDFormat +
        " Product ID: " + productIDFormat ;
    }
}
