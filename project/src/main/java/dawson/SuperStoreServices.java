package dawson;

import java.sql.*;

public class SuperStoreServices {
    private Connection conn;

    public SuperStoreServices(String username, String password) throws SQLException
    {
        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        this.conn = DriverManager.getConnection(url, username, password);
    }
    
    public void close() throws SQLException
    {
        this.conn.close();
    }
    
    public String retreiveAuditLog() throws SQLException, ClassNotFoundException
    {
        String auditLog = "";
        String sql = "{call audit_package.display_audit(?)}";
        try(CallableStatement stmt = this.conn.prepareCall(sql)){
            stmt.registerOutParameter(1, Types.REF_CURSOR);
            stmt.execute();
            ResultSet rs = (ResultSet)stmt.getObject(1);

            while(rs.next()){
                int audit_id = rs.getInt(1);
                String table_changed = rs.getString(2);
                Date date_changed = rs.getDate(3);
                String action = rs.getString(4);

                auditLog += "Audit id: " + audit_id +
                "\tTable changed: " + table_changed +
                "\tDate changed: " + date_changed +
                "\tAction: " + action + "\n";
            }
        }

        return auditLog;
    }
    
    public static String retreiveAuditLog(Connection conn)
    {
        String auditLog = "";
        String sql = "{call audit_package.display_audit(?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.registerOutParameter(1, Types.REF_CURSOR);
            stmt.execute();
            ResultSet rs = (ResultSet)stmt.getObject(1);

            while(rs.next()){
                int audit_id = rs.getInt(1);
                String table_changed = rs.getString(2);
                Date date_changed = rs.getDate(3);
                String action = rs.getString(4);

                String auditIDFormat = String.format("%2s", audit_id);
                String tableChanegedFormat = String.format("%17s", table_changed);
                String dateChagedFormat = String.format("%10s", date_changed);
                String actionFormat = String.format("%10s", action);

                auditLog += "Audit id: " + auditIDFormat +
                "\tTable changed: " + tableChanegedFormat +
                "\tDate changed: " + dateChagedFormat +
                "\tAction: " + actionFormat + "\n";
            }
        }
        catch(Exception e)
        {
            System.out.println("\n--Error! Unable to get audit log from Database!--\n");
        }

        return auditLog;
    }

    public Connection getConn() {
        return conn;
    }
}
