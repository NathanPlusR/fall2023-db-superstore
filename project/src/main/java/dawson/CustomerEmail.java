package dawson;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomerEmail {
    private String email;
    private int customer_id;
    
    public CustomerEmail(String email, int customer_id)
    {
        this.email = email;
        this.customer_id = customer_id;
    }

    public void AddToDatabase(Connection conn) throws SQLException, ClassNotFoundException
    {
        String sql = "{call customer_email_package.add_customer_email(?, ?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.setObject(1, this.email);
            stmt.setObject(2, this.customer_id);
            stmt.execute();
        }
    }

    public static List<CustomerEmail> selectEmailsByCustomer (Connection conn, int customer_id) throws SQLException, ClassNotFoundException
    {
        List<CustomerEmail> emails = new ArrayList<CustomerEmail>();
        String sql = "{call customer_email_package.display_customer_emails(?, ?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.registerOutParameter(2, Types.REF_CURSOR);
            stmt.setInt(1, customer_id);
            stmt.execute();
            ResultSet rs = (ResultSet)stmt.getObject(2);

            while(rs.next()){
                String email = rs.getString(1);
                customer_id = rs.getInt(2);

                emails.add(new CustomerEmail(email, customer_id));
            }
        }

        return emails;
    }

    public int getCustomer_id() {
        return customer_id;
    }
    public String getEmail() {
        return email;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }

    public String toString()
    {
        String customerIDFormat = String.format("%2s", this.customer_id);
        String customerEmailFormat = String.format("%50s", this.email);
        return "Customer: " + customerIDFormat + "\tEmail: " + customerEmailFormat;
    }
}
