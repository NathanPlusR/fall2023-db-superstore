package dawson;

import java.sql.CallableStatement;
import java.sql.Connection;

public class Store {
      private int storeID;
      private String name;

      public Store(String name) {
            this.name = name;
      }

      public int getStoreID() {
            return this.storeID;
      }

      public String getName() {
            return this.name;
      }

      public void addStore(Connection conn) {
            try {
                  String sql = "{call store_package.add_store(?)}";
                  CallableStatement statement = conn.prepareCall(sql);
                  statement.setString(1, this.name);
                  statement.execute();
            } catch (Exception e) {
                  System.out.println("Could not add store to database or it already exists");
            }
      }

      public static void removeStore(Connection conn, int storeID) {
            try {
                  String sql = "{call store_package.remove_store(?)}";
                  CallableStatement statement = conn.prepareCall(sql);
                  statement.setInt(1, storeID);
                  statement.execute();
            } catch (Exception e) {
                  System.out.println("Could not remove store or doesn't exist");
            }
      }

      public static void updateStore(Connection conn, int storeID, String name) {
            try {
                  String sql = "{call store_package.update_store(?, ?)}";
                  CallableStatement statement = conn.prepareCall(sql);
                  statement.setInt(1, storeID);
                  statement.setString(2, name);
                  statement.execute();
            } catch (Exception e) {
                  System.out.println("Could not update store data or doesn't exist");
            }
      }

      public static void addStoreCategory(Connection conn, String category, int storeID) {
            try {
                  String sql = "{call store_category_package.add_store_category(?, ?)}";
                  CallableStatement statement = conn.prepareCall(sql);
                  statement.setString(1, category);
                  statement.setInt(2, storeID);
                  statement.execute();
            } catch (Exception e) {
                  System.out.println("Could not add store category or it already exists");
            }
      }

      public static void removeStoreCategory(Connection conn, String category, int storeID) {
            try {
                  String sql = "{call store_category_package.remove_store_category(?, ?)}";
                  CallableStatement statement = conn.prepareCall(sql);
                  statement.setString(1, category);
                  statement.setInt(2, storeID);
                  statement.execute();
            } catch (Exception e) {
                  System.out.println("Could not remove store category or it never existed");
            }
      }

      public static void addStoreProduct(Connection conn, int storeID, int productID, int price){
            try {
                  String sql = "{call store_price_package.add_store_price(?, ?, ?)}";
                  CallableStatement statement = conn.prepareCall(sql);
                  statement.setInt(1, storeID);
                  statement.setInt(2, productID);
                  statement.setInt(3, price);
                  statement.execute();
            } catch (Exception e) {
                  System.out.println("Could not add store product or it already exists");
            }
      }

      public static void removeStoreProduct(Connection conn, int storeID, int productID){
            try {
                  String sql = "{call store_price_package.remove_store_price(?, ?)}";
                  CallableStatement statement = conn.prepareCall(sql);
                  statement.setInt(1, storeID);
                  statement.setInt(2, productID);
                  statement.execute();
            } catch (Exception e) {
                  System.out.println("Could not remove store product or it never existed");
            }
      }

      public static void updateStoreProductPrice(Connection conn, int storeID, int productID, int price){
            try {
                  String sql = "{call store_price_package.update_store_price(?, ?, ?)}";
                  CallableStatement statement = conn.prepareCall(sql);
                  statement.setInt(1, storeID);
                  statement.setInt(2, productID);
                  statement.setInt(3, price);
                  statement.execute();
            } catch (Exception e) {
                  System.out.println("Could not update store product's price or it never existed");
            }
      }
}
