package dawson;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Application {
    public static Connection conn;

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        SuperStoreServices manager = loginToDatabase();
        conn = manager.getConn();
        categorySelectingManager();
        manager.close();
        System.out.println("\nThank you for using our Database!\nHave a nice day!");
    }

    public static SuperStoreServices loginToDatabase() {
        SuperStoreServices manager = null;
        boolean validInput = false;

        while (!validInput) {
            try {
                Scanner scan = new Scanner(System.in);
                System.out.println("\nEnter your username: ");
                String username = scan.nextLine();
                System.out.println("Enter your password: ");
                String password = scan.nextLine();
                manager = new SuperStoreServices(username, password);
                validInput = true;
            } catch (Exception e) {
                System.out.println("\nError! Your login info wasn't valid! Please try again!\n");
            }
        }
        return manager;
    }

    public static int getChosenOption(String question, String[] optionList) {
        Scanner scan = new Scanner(System.in);

        boolean validInput = false;
        int selectedOptionID = 0;
        while (!validInput) {
            System.out.println("\n" + question);
            System.out.println("\n0: Go Back");
            for (int i = 0; i < optionList.length; i++) {
                System.out.println((i + 1) + ": " + optionList[i]);
            }

            System.out.println("\nEnter the ID of the selected option:");
            try {
                selectedOptionID = Integer.parseInt(scan.nextLine());
                if (selectedOptionID < 0 || selectedOptionID > optionList.length) {
                    System.out.println("\nInvalid option ID!");
                } else {
                    validInput = true;
                }
            } catch (Exception e) {
                System.out.println("\nNot an option!");
            }
        }
        return selectedOptionID;
    }

    public static void categorySelectingManager() {
        String question = "Which category would you like to peform actions with";
        String[] options = new String[] { "Customers", "Products", "Warehouses", "Orders", "Reviews", "Display Audit Logs"};
        int chosenCategory = getChosenOption(question, options);

        switch (chosenCategory) {
            case 0:
                System.exit(0);
                break;
            case 1:
                customerActionsManager();
                break;
            case 2:
                productActionsManager();
                break;
            case 3:
                warehouseActionsManager();
                break;
            case 4:
                orderActionsManager();
                break;
            case 5:
                reviewActionsManager();
                break;
            case 6:
                System.out.println("\n"+SuperStoreServices.retreiveAuditLog(conn));
            default:
                categorySelectingManager();
                break;
        }
    }

    public static String[] getUserInputPerField(String requirementsQuestion, String[] fieldList) {
        Scanner scan = new Scanner(System.in);

        String[] inputPerField = new String[fieldList.length];

        System.out.println(requirementsQuestion);
        for (int i = 0; i < fieldList.length; i++) {
            System.out.println("Please enter value for: " + fieldList[i]);
            inputPerField[i] = scan.nextLine();
        }
        return inputPerField;
    }

    public static <T> void printList(List<T> list) {
        for (T listItem : list) {
            System.out.println(listItem);
        }
    }

    public static void customerActionsManager() {
        String question = "Which action would you like to perform with customers?";
        String[] customerActions = new String[] { "Add Customer", "Get Customer", "Get Customer List",
                "Existing Customer Actions..." };
        int chosenAction = getChosenOption(question, customerActions);
        String requirementsQuestion = "Enter customer's details";
        switch (chosenAction) {
            case 0:
                categorySelectingManager();
                break;
            case 1:
                addCustomer(requirementsQuestion);
                break;
            case 2:
                getCustomer(requirementsQuestion);
                break;
            case 3:
                getCustomerList();
                break;
            case 4:
                String questionExistingCust = "Which action would you like to perform with an existing customer?";
                String[] existingCustomerActions = new String[] { "Get Customer List",
                        "Add Customer Address", "Get Customer's Addresses",
                        "Add Customer Email",
                         "Get Customer's Emails" };
                int existingCustChosenAction = getChosenOption(questionExistingCust, existingCustomerActions);

                switch (existingCustChosenAction) {
                    case 0:
                        customerActionsManager();
                        break;
                    case 1:
                        getCustomerList();
                        break;
                    case 2:
                        addCustomerAddress(requirementsQuestion);
                        break;
                    case 3:
                        getCustomerAddresses(requirementsQuestion);
                        break;
                    case 4:
                        addCustomerEmail(requirementsQuestion);
                        break;
                    case 5:
                        getCustomerEmails(requirementsQuestion);
                        break;
                }
        }
        customerActionsManager();
    }

    public static void addCustomer(String requirementsQuestion) {
        String[] fieldList = new String[] { "first name", "last name" };
        String[] userInputPerFields = getUserInputPerField(requirementsQuestion, fieldList);
        Customer customer = new Customer(userInputPerFields[0], userInputPerFields[1]);
        try {
            customer.AddToDatabase(conn);
            System.out.println("\nSuccessfully added customer!\n");
        } catch (Exception e) {
            System.out.println("\n--Error! Unable to add customer to Database!--\n");
        }
    }

    public static void getCustomer(String requirementsQuestion) {
        String[] fieldList = new String[] { "customer ID" };
        String[] userInputPerFields = getUserInputPerField(requirementsQuestion, fieldList);

        try {
            Customer customer = Customer.selectCustomerByID(conn, Integer.parseInt(userInputPerFields[0]));
            if (customer == null) { throw new NullPointerException(); }

            if (Customer.selectFlaggedCustomers(conn).contains(customer)) {
                System.out.println(customer+ "\t--Flagged Customer--");
            }
            else {
                System.out.println(customer);
            }
        } catch (Exception e) {
            System.out.println("\n--Error! Unable to retreive that customer from Database!--\n");
        }
    }

    public static void getCustomerList() {
        try {
            List<Customer> customerList = Customer.selectCustomersList(conn);
            List<String> finalCustomerList = new ArrayList<String>();
            List<Customer> flaggedCustomers = Customer.selectFlaggedCustomers(conn);

            for(Customer customer : customerList){
                if (flaggedCustomers.contains(customer))
                {
                    finalCustomerList.add(customer.toString() + "\t--Flagged Customer--");
                }
                else
                {
                    finalCustomerList.add(customer.toString());
                }
            }
            printList(finalCustomerList);

        } catch (Exception e) {
            System.out.println("\n--Error! Unable to retreive customers from Database!--\n");
        }
    }

    public static void addCustomerAddress(String requirementsQuestion) {
        String[] fieldList = new String[] { "customer ID", "address" };
        String[] userInputPerFields = getUserInputPerField(requirementsQuestion, fieldList);
        CustomerAddress customerAddress = new CustomerAddress(userInputPerFields[1],
                Integer.parseInt(userInputPerFields[0]));
        try {
            customerAddress.AddToDatabase(conn);
            System.out.println("\nSuccessfully added customer address!\n");
        } catch (Exception e) {
            System.out.println("\n--Error! Unable to add to databse!--\n");
        }
    }

    public static void getCustomerAddresses(String requirementsQuestion) {
        String[] fieldList = new String[] { "customer ID" };
        String[] userInputPerFields = getUserInputPerField(requirementsQuestion, fieldList);

        try {
            List<CustomerAddress> customerAddresseList = CustomerAddress.selectAddressesByCustomer(conn, Integer.parseInt(userInputPerFields[0]));
            if (customerAddresseList.size() == 0) { throw new NullPointerException(); }
            printList(customerAddresseList);
        } catch (Exception e) {
            System.out.println("\n--Error! Unable to retrieve addresses from databse!--\n");
        }
    }

    public static void addCustomerEmail(String requirementsQuestion) {
        String[] fieldList = new String[] { "customer ID", "email" };
        String[] userInputPerFields = getUserInputPerField(requirementsQuestion, fieldList);

        try {
            CustomerEmail customerEmail = new CustomerEmail(userInputPerFields[1],
                    Integer.parseInt(userInputPerFields[0]));
            customerEmail.AddToDatabase(conn);
        } catch (Exception e) {
            System.out.println("\n--Error! Unable to add to databse!--\n");
        }
    }

    public static void getCustomerEmails(String requirementsQuestion) {
        String[] fieldList = new String[] { "customer ID" };
        String[] userInputPerFields = getUserInputPerField(requirementsQuestion, fieldList);

        try {
            List<CustomerEmail> customerEmailList = CustomerEmail.selectEmailsByCustomer(conn, Integer.parseInt(userInputPerFields[0]));
            if (customerEmailList.size() == 0) { throw new NullPointerException(); }
            printList(customerEmailList);
        } catch (Exception e) {
            System.out.println("\n--Error! Unable to retrieve emails from databse!--\n");
        }
    }

    public static void productActionsManager() {
        String question = "Which action would you like to perform with products?";
        String[] productActions = new String[] { "Add Product", "Display All Products", "Display Products By Category", "Display Products' Stock", "Existing Products Actions..." };
        int chosenAction = getChosenOption(question, productActions);
        String requirementsQuestion = "Enter product's details";
        switch (chosenAction) {
            case 0:
                categorySelectingManager();
                break;
            case 1:
                addProduct(requirementsQuestion);
                break;
            case 2:
                displayProducts(requirementsQuestion);
                break;
            case 3:
                displayProductsByCategory(requirementsQuestion);
                break;
            case 4:
                displayAllProductStock();
                break;
            case 5:
                String questionExistingProd = "Which action would you like to perform with an existing product?";
                String[] existingProductActions = new String[] { "Display Products",
                        "Delete Product", "Update Product" };
                int existingCustChosenAction = getChosenOption(questionExistingProd, existingProductActions);

                switch (existingCustChosenAction) {
                    case 0:
                        productActionsManager();
                        break;
                    case 1:
                        displayProducts(requirementsQuestion);
                        break;
                    case 2:
                        deleteProduct(requirementsQuestion);
                        break;
                    case 3:
                        updateProduct(requirementsQuestion);
                        break;
                }
        }
        productActionsManager();
    }

    public static void addProduct(String requirementsQuestion) {
        String[] fieldList = new String[] { "product name", "category", "price" };
        String[] userInputPerFields = getUserInputPerField(requirementsQuestion, fieldList);
        Product product = new Product(userInputPerFields[0], userInputPerFields[1],
                Double.parseDouble(userInputPerFields[2]));
        try {
            product.AddToDatabase(conn);
            System.out.println("\nSuccessfully added product to database!\n");
        } catch (Exception e) {
            System.out.println("\n--Error! Unable to add product to databse!--\n");
        }
    }

    public static void displayProducts(String requirementsQuestion) {
        try {
            List<Product> productList = Product.selectProducts(conn);
            List<String> finalProductList = new ArrayList<String>();
            for(Product product : productList){
                String stockFormat = String.format("%9s", product.getTotalStock(conn));
                String avgScoreFormat = String.format("%3s", Review.getAverageReviewScore(conn, product.getProduct_id()));
                String timesOrderedFormat = String.format("%5s", product.timesProductWasOrdered(conn));
                finalProductList.add(product.toString() + "     Stock: " + stockFormat
                + "      Avg Score: " + avgScoreFormat
                + "      Times Ordered:  " + timesOrderedFormat);
            }
            printList(finalProductList);
        } 
        catch (Exception e) {
            System.out.println("\n--Error! Unable to retrieve products from databse!--\n");
        }
    }

    public static void displayProductsByCategory(String requirementsQuestion) {
        String[] fieldList = new String[] { "category" };
        String[] userInputPerFields = getUserInputPerField(requirementsQuestion, fieldList);
        try {
            List<Product> productList = Product.selectProducts(conn, userInputPerFields[0]);
            List<String> finalProductList = new ArrayList<String>();
            for(Product product : productList){
                String stockFormat = String.format("%9s", product.getTotalStock(conn));
                String avgScoreFormat = String.format("%3s", Review.getAverageReviewScore(conn, product.getProduct_id()));
                String timesOrderedFormat = String.format("%5s", product.timesProductWasOrdered(conn));

                finalProductList.add(product.toString() + "     Stock: " + stockFormat
                + "      Avg Score: " + avgScoreFormat
                + "      Times Ordered:  " +timesOrderedFormat);
            }
            printList(finalProductList);
        } catch (Exception e) {
            System.out.println("\n--Error! Unable to retrieve products from databse!--\n");
        }
    }

    public static void deleteProduct(String requirementsQuestion) {
        String[] fieldList = new String[] { "product ID" };
        String[] userInputPerFields = getUserInputPerField(requirementsQuestion, fieldList);

        try {
            Product.removeFromDatabase( conn, Integer.parseInt(userInputPerFields[0]));
            System.out.println("\nSuccessfully removed product from database!\n");
        } catch (Exception e) {
            System.out.println("\n--Error! Unable to remove from databse!--\n");
        }
    }

    public static void updateProduct(String requirementsQuestion) {
        String[] fieldList = new String[] { "product ID", "product name", "category", "price" };
        String[] userInputPerFields = getUserInputPerField(requirementsQuestion, fieldList);
        Product product = new Product(Integer.parseInt(userInputPerFields[0]), userInputPerFields[1],
                userInputPerFields[2], Double.parseDouble(userInputPerFields[3]));
        try {
            product.updateProduct(conn);
            System.out.println("\nSuccessfully removed customer address from database!\n");
        } catch (Exception e) {
            System.out.println("\n--Error! Unable to update product!--\n");
        }
    }

    public static void displayAllProductStock() {
        try {
            List<Product> productList = Product.selectProducts(conn);

            for(Product product : productList){
                // int getTotalStock = Product.getTotalStock( conn, product.getProduct_id());
                String idFormat = String.format("%2s", product.getProduct_id());
                String nameFormat = String.format("%40s", product.getName());
                String stockFormat = String.format("%9s", product.getTotalStock(conn));
            System.out.println("Id: " + idFormat + "   Product Name: " + nameFormat + "   Stock: " + stockFormat);
        }
        } catch (Exception e) {
            System.out.println("\n--Error! Unable to retrieve information from databse!--\n");
        }
    }

    public static void orderActionsManager() {
        String question = "Which action would you like to perform with orders?";
        String[] orderActions = new String[] { "Add Order", "Display Orders", "Display Orders by Store", "Existing Orders Actions" };
        int chosenAction = getChosenOption(question, orderActions);
        String requirementsQuestion = "Enter order's details";
        switch (chosenAction) {
            case 0:
                categorySelectingManager();
                break;
            case 1:
                addOrder(requirementsQuestion);
                break;
            case 2:
                displayOrders();
                break;
            case 3:
                displayOrdersByStore();
                break;
            case 4:
                String questionExistingOrder = "Which action would you like to perform with an existing order?";
                String[] existingOrderActions = new String[] { "Display Orders",
                        "Delete Order" };
                int existingCustChosenAction = getChosenOption(questionExistingOrder, existingOrderActions);

                switch (existingCustChosenAction) {
                    case 0:
                        orderActionsManager();
                        break;
                    case 1:
                        displayOrders();
                        break;
                    case 2:
                        deleteOrder(requirementsQuestion);
                        break;
                }
        }
        orderActionsManager();
    }

    public static void addOrder(String requirementsQuestion) {
        String[] fieldList = new String[] { "quantitiy", "email", "address", "price", "store ID",
                "product ID" };
        String[] userInputPerFields = getUserInputPerField(requirementsQuestion, fieldList);
        try {
            int quantity = Integer.parseInt(userInputPerFields[0]);
            String email = userInputPerFields[1];
            String address = userInputPerFields[2];
            double price = Double.parseDouble(userInputPerFields[3]);
            int store_id = Integer.parseInt(userInputPerFields[4]);
            int product_id = Integer.parseInt(userInputPerFields[5]);
            
            Orders order = new Orders( quantity, email, address, price, store_id, product_id);
            try {
                order.AddToDatabase(conn);
                System.out.println("\nSuccessfully added order!\n");
            } catch (Exception e) {
                System.out.println("\n--Error! Unable to add that order!--\n");
            }
        } catch (Exception e) {
            System.out.println("\n--Error! You seem to have made a mistake when inputting data, try again please!--\n");
        }
        
    }

    public static void displayOrders() {
        try {
            List<Orders> orderList = Orders.selectOrders(conn);
            printList(orderList);
        } catch (Exception e) {
            System.out.println("\n--Error! Unable to retrieve orders from Database!--\n");
        }
    }

    public static void displayOrdersByStore() {
        String[] fieldList = new String[] { "Store ID" };
        String[] userInputPerFields = getUserInputPerField("What store ID Would you like to search by?", fieldList);
        try {
            List<Orders> orderList = Orders.selectOrdersByStore(conn, Integer.parseInt(userInputPerFields[0]));
            printList(orderList);
        } catch (Exception e) {
            System.out.println("\n--Error! Unable to retrieve orders from Database!--\n");
        }
    }

    public static void deleteOrder(String requirementsQuestion) {
        String[] fieldList = new String[] { "order ID" };
        String[] userInputPerFields = getUserInputPerField(requirementsQuestion, fieldList);
        try {
            Orders.removeFromDatabase(conn, Integer.parseInt(userInputPerFields[0]));
            System.out.println("\nSuccessfully removed order from database!\n");
        } catch (Exception e) {
            System.out.println("\n--Error! Unable to remove order from database!--\n");
        }
    }

    public static void warehouseActionsManager() {
        String question = "Which action would you like to perform with warehouses?";
        String[] warehouseActions = new String[] { "Add Warehouse", "Display Warehouses",
                "Existing Warehouses Actions", "Existing Warehouse Stock Management" };
        int chosenAction = getChosenOption(question, warehouseActions);
        String requirementsQuestion = "Enter warehouse's details";

        String questionExistingOrder;
        String[] existingOrderActions;
        int existingCustChosenAction;

        switch (chosenAction) {
            case 0:
                categorySelectingManager();
                break;
            case 1:
                addWarehouse(requirementsQuestion);
                break;
            case 2:
                displayWarehouses();
                break;
            case 3:
                questionExistingOrder = "Which action would you like to perform with an existing warehouse?";
                existingOrderActions = new String[] { "Display Warehouses", "Delete Warehouse", "Update Warehouse Info"};
                existingCustChosenAction = getChosenOption(questionExistingOrder, existingOrderActions);

                switch (existingCustChosenAction) {
                    case 0:
                        warehouseActionsManager();
                        break;
                    case 1:
                        displayWarehouses();
                        break;
                    case 2:
                        deleteWarehouse(requirementsQuestion);
                        break;
                    case 3:
                        updateWarehouse(requirementsQuestion);
                        break;
                }
                break;
            case 4:
                questionExistingOrder = "How would you like to change the stock of an existing warehouse?";
                existingOrderActions = new String[] {"Add Product Stock", "Update Product Stock", "Remove Product Stock"};
                existingCustChosenAction = getChosenOption(questionExistingOrder, existingOrderActions);

                switch (existingCustChosenAction) {
                    case 0:
                        warehouseActionsManager();
                        break;
                    case 1:
                        addProductStock(requirementsQuestion);
                        break;
                    case 2:
                        updateProductStock(requirementsQuestion);
                        break;
                    case 3:
                        removeProductStock(requirementsQuestion);
                        break;     
                } 
                break;                 
        }
        warehouseActionsManager();
    }

    public static void addWarehouse(String requirementsQuestion) {
        String[] fieldList = new String[] { "warehouse name", "address" };
        String[] userInputPerFields = getUserInputPerField(requirementsQuestion, fieldList);
        Warehouse warehouse = new Warehouse(userInputPerFields[0], userInputPerFields[1]);
        try {
            warehouse.addWarehouse(conn);
        } catch (Exception e) {
            System.out.println("\n--Error! Unable to add warehouse to database!--\n");
        }
    }

    public static void displayWarehouses() {
        try {
            List<Warehouse> warehouseList = Warehouse.getAllWarehouses(conn);
            printList(warehouseList);
        } catch (Exception e) {
            System.out.println("\n--Error! Unable to retreive info from database!--\n");
            System.out.println(e.getMessage());
        }
    }

    public static void deleteWarehouse(String requirementsQuestion) {
        String[] fieldList = new String[] { "warehouse ID" };
        String[] userInputPerFields = getUserInputPerField(requirementsQuestion, fieldList);
        try {
            Warehouse.removeWarehouse(conn, Integer.parseInt(userInputPerFields[0]));
            System.out.println("\nSuccessfully removed warehouse from database!\n");
        } catch (Exception e) {
            System.out.println("\n--Error! Unable to remove warehouse from database!--\n");
        }
    }

    public static void updateWarehouse(String requirementsQuestion) {
        String[] fieldList = new String[] { "warehouse ID", "warehouse name", "address" };
        String[] userInputPerFields = getUserInputPerField(requirementsQuestion, fieldList);
        try {
            Warehouse.updateWarehouseInfo(conn, Integer.parseInt(userInputPerFields[0]), userInputPerFields[1], userInputPerFields[2]);
            System.out.println("\nSuccessfully updated warehouse!\n");
        } catch (Exception e) {
            System.out.println("\n--Error! Unable to update warehouse!--\n");
        }
    }

    public static void addProductStock(String requirementsQuestion) {
        String[] fieldList = new String[] { "warehouse ID", "product ID", "quantity" };
        String[] userInputPerFields = getUserInputPerField(requirementsQuestion, fieldList);
        try {
            Warehouse.addProductStock(conn, Integer.parseInt(userInputPerFields[0]), Integer.parseInt(userInputPerFields[1]), Integer.parseInt(userInputPerFields[2]));
            System.out.println("\nSuccessfully added stock for product!\n");
        } catch (Exception e) {
            System.out.println("\n--Error! Unable to add stock!--\n");
        }
    }

    public static void updateProductStock(String requirementsQuestion) {
        String[] fieldList = new String[] { "warehouse ID", "product ID", "quantity change (add or substract)" };
        String[] userInputPerFields = getUserInputPerField(requirementsQuestion, fieldList);
        try {
            Warehouse.updateProductStock(conn, Integer.parseInt(userInputPerFields[0]), Integer.parseInt(userInputPerFields[1]), Integer.parseInt(userInputPerFields[2]));
            System.out.println("\nSuccessfully updated stock!\n");
        } catch (Exception e) {
            System.out.println("\n--Error! Unable to update stock!--\n");
        }
    }

    public static void removeProductStock(String requirementsQuestion) {
        String[] fieldList = new String[] { "warehouse ID", "product ID" };
        String[] userInputPerFields = getUserInputPerField(requirementsQuestion, fieldList);
        try {
            Warehouse.removeProductStock(conn, Integer.parseInt(userInputPerFields[0]), Integer.parseInt(userInputPerFields[1]));
            System.out.println("\nSuccessfully removed stock!\n");
        } catch (Exception e) {
            System.out.println("\n--Error! Unable to remove stock!--\n");
        }
    }

    public static void reviewActionsManager() {
        String question = "Which action would you like to perform with reviews?";
        String[] reviewActions = new String[] { "Add Review", "Display Flagged Reviews",
                "Existing Reviews Actions" };
        int chosenAction = getChosenOption(question, reviewActions);
        String requirementsQuestion = "Enter review's details";
        switch (chosenAction) {
            case 0:
                categorySelectingManager();
                break;
            case 1:
                addReview(requirementsQuestion);
                break;
            case 2:
                displayFlaggedReviews();
                break;
            case 3:
                String questionExistingReview = "Which action would you like to perform with an existing review?";
                String[] existingReviewActions = new String[] { "Display Flagged Reviews",
                        "Delete Review", "Update Flagged Review Description", "Update Review's Flags" };
                int existingReviewChosenAction = getChosenOption(questionExistingReview, existingReviewActions);

                switch (existingReviewChosenAction) {
                    case 0:
                        warehouseActionsManager();
                        break;
                    case 1:
                        displayFlaggedReviews();
                        break;
                    case 2:
                        deleteReview(requirementsQuestion);
                        break;
                    case 3:
                        updateFlaggedReviewDescription(requirementsQuestion);
                        break;
                    case 4:
                        updateReviewFlags(requirementsQuestion);
                        break;
                }
        }
        reviewActionsManager();
    }

    public static void addReview(String requirementsQuestion) {
        String[] fieldList = new String[] { "customer ID", "product ID", "score", "description" };
        String[] userInputPerFields = getUserInputPerField(requirementsQuestion, fieldList);
        Review review = new Review(Integer.parseInt(userInputPerFields[2]), userInputPerFields[3], Integer.parseInt(userInputPerFields[0]), Integer.parseInt(userInputPerFields[1]));
        try {
            review.AddToDatabase(conn);
            System.out.println("\nSuccessfully added review!\n");
        } catch (Exception e) {
            System.out.println("\n--Error! Unable to add review!--\n");
        }
    }

    public static void displayFlaggedReviews() {
        try {
            List<Review> reviewList = Review.selectFlaggedReviews(conn);
            printList(reviewList);
        } catch (Exception e) {
            System.out.println("\n--Error! Unable to retrieve customers!--\n");
        }
    }

    public static void deleteReview(String requirementsQuestion) {
        String[] fieldList = new String[] { "review ID" };
        String[] userInputPerFields = getUserInputPerField(requirementsQuestion, fieldList);
        try {
            Review.removeFromDatabase( conn, Integer.parseInt(userInputPerFields[0]));
            System.out.println("\nSuccessfully removed review!\n");
        } catch (Exception e) {
            System.out.println("\n--Error! Unable to remove review!--\n");
        }
    }

    public static void updateFlaggedReviewDescription(String requirementsQuestion) {
        String[] fieldList = new String[] { "review ID", "description" };
        String[] userInputPerFields = getUserInputPerField(requirementsQuestion, fieldList);
        try {
            Review.UpdateDescription( conn, Integer.parseInt(userInputPerFields[0]), userInputPerFields[1]);
            System.out.println("\nSuccessfully changed review description!\n");
        } catch (Exception e) {
            System.out.println("\n--Error! Unable to change review description!--\n");
        }
    }

    public static void updateReviewFlags(String requirementsQuestion) {
        String[] fieldList = new String[] { "review ID", "flags" };
        String[] userInputPerFields = getUserInputPerField(requirementsQuestion, fieldList);
        try {
            Review.UpdateFlags( conn, Integer.parseInt(userInputPerFields[0]), Integer.parseInt(userInputPerFields[1]));
            System.out.println("\nSuccessfully changed review flags!\n");
        } catch (Exception e) {
            System.out.println("\n--Error! Unable to change review flags!--\n");
        }
    }
}
