package dawson;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Review implements SQLData{
    private int review_id;
    private int score;
    private String description;
    private int flag;
    private int customer_id;
    private int product_id;
    public String typeName;


    public Review()
    {

    }

    public Review(
        int score,
        String description,
        int customer_id,
        int product_id)
    {
        this.review_id = -1;
        this.score = score;
        this.description = description;
        this.flag = 0;
        this.customer_id = customer_id;
        this.product_id = product_id;
        this.typeName = "REVIEW_OBJECT";
    }

    private Review(
        int review_id,
        int score,
        String description,
        int flag,
        int customer_id,
        int product_id)
    {
        this.review_id = review_id;
        this.score = score;
        this.description = description;
        this.flag = flag;
        this.customer_id = customer_id;
        this.product_id = product_id;
        this.typeName = "REVIEW_OBJECT";
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException
    {
        stream.writeInt(this.review_id);
        stream.writeInt(this.score);
        stream.writeString(this.description);
        stream.writeInt(this.flag);
        stream.writeInt(this.customer_id);
        stream.writeInt(this.product_id);
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        this.review_id = stream.readInt();
        this.score = stream.readInt();
        this.description = stream.readString();
        this.flag = stream.readInt();
        this.customer_id = stream.readInt();
        this.product_id =stream.readInt();
    }

    @Override
    public String getSQLTypeName() throws SQLException
    {
        return this.typeName;
    }

    public void AddToDatabase(Connection conn) throws SQLException, ClassNotFoundException
    {
        Map map = conn.getTypeMap();
        conn.setTypeMap(map);
        map.put(this.getSQLTypeName(), Class.forName("dawson.Review"));
        String sql = "{call review_package.add_review(?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.setObject(1, this);
            stmt.execute();
        }
    }

    public void removeFromDatabase(Connection conn) throws SQLException, ClassNotFoundException
    {
        String sql = "{call review_package.delete_review(?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.setObject(1, this.review_id);
            stmt.execute();
        }
    }

    public static void removeFromDatabase(Connection conn, int review_id) throws SQLException, ClassNotFoundException
    {
        String sql = "{call review_package.delete_review(?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.setObject(1, review_id);
            stmt.execute();
        }
    }

    public static double getAverageReviewScore(Connection conn, int product_id) throws SQLException, ClassNotFoundException
    {
        double average = 0;
        String sql = "{? = call review_package.product_average_reviews(?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.registerOutParameter(1, Types.DOUBLE);
            stmt.setObject(2, product_id);
            stmt.execute();
            average = (double)stmt.getObject(1);
        }

        return average;
    }

    public static List<Review> selectFlaggedReviews(Connection conn) throws SQLException, ClassNotFoundException
    {
        List<Review> flagged = new ArrayList<Review>();
        String sql = "{? = call review_package.show_flagged_reviews()}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.registerOutParameter(1, Types.REF_CURSOR);
            stmt.execute();
            ResultSet rs = (ResultSet)stmt.getObject(1);

            while(rs.next()){
                int review_id = rs.getInt(1);
                int score = rs.getInt(2);
                String description = rs.getString(3);;
                int flag = rs.getInt(4);
                int customer_id = rs.getInt(5);
                int product_id = rs.getInt(6);
  
                flagged.add(new Review(review_id, score, description, flag, customer_id, product_id));
            }
        }

        return flagged;
    }

    public static void UpdateDescription(Connection conn, int review_id, String description) throws SQLException, ClassNotFoundException
    {
        String sql = "{call review_package.update_description(?, ?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.setObject(1, review_id);
            stmt.setObject(2, description);
            stmt.execute();
        }
    }

    public void UpdateDescription(Connection conn, String newDescription) throws SQLException, ClassNotFoundException
    {
        String sql = "{call review_package.update_description(?, ?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.setObject(1, this.review_id);
            stmt.setObject(2, newDescription);
            stmt.execute();
        }
    }

    public static void UpdateFlags(Connection conn, int review_id, int flags) throws SQLException, ClassNotFoundException
    {
        String sql = "{call review_package.update_flags(?, ?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.setObject(1, review_id);
            stmt.setObject(2, flags);
            stmt.execute();
        }
    }

    public void UpdateFlags(Connection conn, int newFlags) throws SQLException, ClassNotFoundException
    {
        String sql = "{call review_package.update_flags(?, ?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.setObject(1, this.review_id);
            stmt.setObject(2, newFlags);
            stmt.execute();
        }
    }

    public int getCustomer_id() {
        return customer_id;
    }
    public String getDescription() {
        return description;
    }
    public int getFlag() {
        return flag;
    }
    public int getProduct_id() {
        return product_id;
    }
    public int getReview_id() {
        return review_id;
    }
    public int getScore() {
        return score;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setFlag(int flag) {
        this.flag = flag;
    }
    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }
    public void setReview_id(int review_id) {
        this.review_id = review_id;
    }
    public void setScore(int score) {
        this.score = score;
    }

    public String toString()
    {
        String reviewIDFormat = String.format("%2s", this.review_id);
        String scoreFormat = String.format("%2s", this.score);
        String descriptionFormat = String.format("%50s", this.description);
        String flagFormat = String.format("%2s", this.flag);
        String storeIDFormat = String.format("%2s", this.customer_id);
        String productIDFormat = String.format("%2s", this.product_id);


        return "Id: " + reviewIDFormat
        + "\tScore: " + scoreFormat
        + "\tDescription: " + descriptionFormat
        + "\tFlag: " + flagFormat
        + "\tCustomer ID: " + storeIDFormat
        + "\tProduct ID: " + productIDFormat;
    }
}