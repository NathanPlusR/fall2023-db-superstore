--Table Creation
create table customer(
    customer_id number(10) generated always as identity primary key,
    fname varchar2(50) not null,
    lname varchar2(50) not null
);

create table customer_email(
    email varchar2(100) primary key,
    customer_id number(10) references customer(customer_id)
);

create table customer_address(
    address varchar2(1000) primary key,
    customer_id number(10) references customer(customer_id)
);

create table product(
    product_id number(10) generated always as identity primary key,
    name varchar2(100) not null,
    category varchar2(100) not null,
    price number(9,2) not null
);

create table review(
    review_id number(10) generated always as identity primary key,
    score number(1) check (score in (1,2,3,4,5)),
    description varchar2(1000),
    flag number(3) check (flag >= 0),
    customer_id number(10) references customer(customer_id),
    product_id number(10) references product(product_id) on delete set null
);

create table store(
    store_id number(10) generated always as identity primary key,
    name varchar2(100) not null
);

create table store_category(
    category varchar2(100),
    store_id number(10) references store(store_id) on delete cascade,
    constraint store_category_composite_key primary key(category, store_id)
);

create table store_price(
    store_id number(10) references store(store_id) on delete cascade,
    product_id number(10) references product(product_id) on delete cascade,
    price number(9,2) not null check(price >= 0),
    constraint store_price_composite_key primary key(store_id, product_id)
);

create table orders(
    order_id number(10) generated always as identity primary key,
    order_date date not null,
    quantity number(5) not null,
    email varchar2(100) references customer_email(email),
    address varchar2(1000) references customer_address(address),
    price number(9,2) not null check(price >= 0),
    store_id number(10) references store(store_id) on delete set null,
    product_id number(10) references product(product_id) on delete set null
);

create table warehouse(
    warehouse_id number(10) generated always as identity primary key,
    name varchar2(50) not null unique,
    address varchar2(100) not null
);

create table warehouse_stock(
    warehouse_id number(10) references warehouse(warehouse_id) on delete cascade,
    product_id number(10) references product(product_id) on delete cascade,
    quantity number(8) not null check(quantity >= 0),
    constraint warehouse_stock_composite_key primary key(warehouse_id, product_id)
);

-- Insert

--customer
INSERT INTO customer (fname, lname) VALUES ('Mahsa', 'Sadeghi');
INSERT INTO customer (fname, lname) VALUES ('Alex', 'Brown');
INSERT INTO customer (fname, lname) VALUES ('Martin', 'Alexandre');
INSERT INTO customer (fname, lname) VALUES ('Daneil', 'Hanne');
INSERT INTO customer (fname, lname) VALUES ('John', 'Boura');
INSERT INTO customer (fname, lname) VALUES ('Ari', 'Brown');
INSERT INTO customer (fname, lname) VALUES ('Amanda', 'Harry');
INSERT INTO customer (fname, lname) VALUES ('Jack', 'Johnson');
INSERT INTO customer (fname, lname) VALUES ('John', 'Belle');
INSERT INTO customer (fname, lname) VALUES ('Martin', 'Li');
INSERT INTO customer (fname, lname) VALUES ('Olivia', 'Smith');
INSERT INTO customer (fname, lname) VALUES ('Noah', 'Garcia');

--customer_email
INSERT INTO customer_email (email, customer_id) VALUES ('msadeghi@dawsoncollege.qc.ca', 1);
INSERT INTO customer_email (email, customer_id) VALUES ('alex@gmail.com', 2);
INSERT INTO customer_email (email, customer_id) VALUES ('marting@yahoo.com', 3);
INSERT INTO customer_email (email, customer_id) VALUES ('daneil@yahoo.com', 4);
INSERT INTO customer_email (email, customer_id) VALUES ('bdoura@gmail.com', 5);
INSERT INTO customer_email (email, customer_id) VALUES ('b.a@gmail.com', 6);
INSERT INTO customer_email (email, customer_id) VALUES ('am.harry@yahioo.com', 7);
INSERT INTO customer_email (email, customer_id) VALUES ('johnson.a@gmail.com', 8);
INSERT INTO customer_email (email, customer_id) VALUES ('ms@gmail.com', 1);
INSERT INTO customer_email (email, customer_id) VALUES ('abcd@yahoo.com', 9);
INSERT INTO customer_email (email, customer_id) VALUES ('m.li@gmail.com', 10);
INSERT INTO customer_email (email, customer_id) VALUES ('smith@hotmail.com', 11);
INSERT INTO customer_email (email, customer_id) VALUES ('g.noah@yahoo.com', 12);

--customer_address
INSERT INTO customer_address (address, customer_id) VALUES ('dawson college, montreal, qeuebe, canada', 1);
INSERT INTO customer_address (address, customer_id) VALUES ('090 boul saint laurent, montreal, quebec, canada', 2);
INSERT INTO customer_address (address, customer_id) VALUES ('brossard, quebec, canada', 3);
INSERT INTO customer_address (address, customer_id) VALUES ('100 atwater street, toronto, canada', 4);
INSERT INTO customer_address (address, customer_id) VALUES ('100 Young street, toronto, canada', 5);
INSERT INTO customer_address (address, customer_id) VALUES ('boul saint laurent, montreal, quebec, canada', 6);
INSERT INTO customer_address (address, customer_id) VALUES ('100 boul saint laurent, montreal, quebec, canada', 7);
INSERT INTO customer_address (address, customer_id) VALUES ('Calgary, Alberta, Canada', 8);
INSERT INTO customer_address (address, customer_id) VALUES ('104 gill street, Toronto, Canada', 1);
INSERT INTO customer_address (address, customer_id) VALUES ('105 Young street, toronto, canada', 9);
INSERT INTO customer_address (address, customer_id) VALUES ('87 boul saint laurent, montreal, quebec, canada', 10);
INSERT INTO customer_address (address, customer_id) VALUES ('76 boul decalthon, laval, quebec, canada', 11);
INSERT INTO customer_address (address, customer_id) VALUES ('22222 happy street, Laval, quebec, canada', 12);

--product
INSERT INTO product (name, category, price)
    VALUES ('laptop ASUS 104S', 'Electronics', 970);
INSERT INTO product (name, category, price)
    VALUES ('apple', 'Grocery', 10);
INSERT INTO product (name, category, price)
    VALUES ('SIMS CD', 'Video Games', 50);
INSERT INTO product (name, category, price)
    VALUES ('orange', 'Grocery', 2);
INSERT INTO product (name, category, price)
    VALUES ('Barbie Movie', 'DVD', 30);
INSERT INTO product (name, category, price)
    VALUES ('LOreal Normal Hair', 'Health', 10);
INSERT INTO product (name, category, price)
    VALUES ('BMW iX Lego', 'Toys', 40);
INSERT INTO product (name, category, price)
    VALUES ('BMW i6', 'Cars', 50000);
INSERT INTO product (name, category, price)
    VALUES ('Truck 500c', 'Vehicle', 856600);
INSERT INTO product (name, category, price)
    VALUES ('paper towel', 'Beauty', 50);
INSERT INTO product (name, category, price)
    VALUES ('plum', 'Grocery', 10);
INSERT INTO product (name, category, price)
    VALUES ('Lamborghini Lego', 'Toys', 80);
INSERT INTO product (name, category, price)
    VALUES ('chicken', 'Grocery', 9.5);
INSERT INTO product (name, category, price)
    VALUES ('pasta', 'Grocery', 13.5);
INSERT INTO product (name, category, price)
    VALUES ('PS5', 'Electronics', 200);
INSERT INTO product (name, category, price)
    VALUES ('tomato', 'Grocery', 10);
INSERT INTO product (name, category, price)
    VALUES ('Train X745', 'Electronics', 90);
INSERT INTO product (name, category, price)
    VALUES ('Monty Python and the Holy Grail', 'Disk', 1000);
INSERT INTO product (name, category, price)
    VALUES ('Amour Plastique', 'Disk', 90);

--review
INSERT INTO review (score, description, flag, customer_id, product_id)
    VALUES (4, 'it was affordable.', 0, 1, 1);
INSERT INTO review (score, description, flag, customer_id, product_id)
    VALUES (3, 'quality was not good', 0, 2, 2);
INSERT INTO review (score, flag, customer_id, product_id)
    VALUES (2, 1, 3, 3);
INSERT INTO review (score, description, flag, customer_id, product_id)
    VALUES (5, 'highly recommend', 0, 4, 4);
INSERT INTO review (score, flag, customer_id, product_id)
    VALUES (1, 0, 2, 5);
INSERT INTO review (score, description, flag, customer_id, product_id)
    VALUES (1, 'did not worth the price', 0, 3, 6);
INSERT INTO review (score, description, flag, customer_id, product_id)
    VALUES (1, 'missing some parts', 0, 1, 7);
INSERT INTO review (score, description, flag, customer_id, product_id)
    VALUES (5, 'trash', 1, 5, 8);
INSERT INTO review (score, flag, customer_id, product_id)
    VALUES (2, 0, 6, 9);
INSERT INTO review (score, flag, customer_id, product_id)
    VALUES (5, 0, 7, 10);
INSERT INTO review (score, flag, customer_id, product_id)
    VALUES (4, 0, 8, 11);
INSERT INTO review (score, flag, customer_id, product_id)
    VALUES (3, 0, 3, 6);
INSERT INTO review (score, description, flag, customer_id, product_id)
    VALUES (1, 'missing some parts', 0, 1, 12);
INSERT INTO review (description, flag, customer_id, product_id)
    VALUES ('great product', 0, 1, 12);
INSERT INTO review (description, flag, customer_id, product_id)
    VALUES ('bad quality', 1, 9, 8);
INSERT INTO review (score, flag, customer_id, product_id)
    VALUES (4, 0, 10, 13);
INSERT INTO review (score, flag, customer_id, product_id)
    VALUES (5, 0, 11, 14);
INSERT INTO review (description, flag, customer_id, product_id)
    VALUES ('worse car i have droven!', 2, 1, 7);
    
--store
INSERT INTO store (name) VALUES ('marche adonis');
INSERT INTO store (name) VALUES ('marche atwater');
INSERT INTO store (name) VALUES ('dawson store');
INSERT INTO store (name) VALUES ('store magic');
INSERT INTO store (name) VALUES ('movie store');
INSERT INTO store (name) VALUES ('super rue chaplain');
INSERT INTO store (name) VALUES ('toys r us');
INSERT INTO store (name) VALUES ('Dealer one');
INSERT INTO store (name) VALUES ('dealer montreal');
INSERT INTO store (name) VALUES ('movie start');
INSERT INTO store (name) VALUES ('star store');
INSERT INTO store (name) VALUES ('betty alley');
    
--store_category
INSERT INTO store_category (category, store_id) VALUES ('Electronics', 1);
INSERT INTO store_category (category, store_id) VALUES ('Grocery', 2);
INSERT INTO store_category (category, store_id) VALUES ('Video Games', 3);
INSERT INTO store_category (category, store_id) VALUES ('Grocery', 4);
INSERT INTO store_category (category, store_id) VALUES ('DVD', 5);
INSERT INTO store_category (category, store_id) VALUES ('Health', 6);
INSERT INTO store_category (category, store_id) VALUES ('Toys', 7);
INSERT INTO store_category (category, store_id) VALUES ('Cars', 8);
INSERT INTO store_category (category, store_id) VALUES ('Vehicle', 9);
INSERT INTO store_category (category, store_id) VALUES ('Beauty', 10);
INSERT INTO store_category (category, store_id) VALUES ('Video Games', 5);
INSERT INTO store_category (category, store_id) VALUES ('DVD', 7);
INSERT INTO store_category (category, store_id) VALUES ('Grocery', 1);
INSERT INTO store_category (category, store_id) VALUES ('Electronics', 11);
INSERT INTO store_category (category, store_id) VALUES ('Disk', 12);
    
--store_price
INSERT INTO store_price (store_id, product_id, price)
    VALUES (1, 1, 970);
INSERT INTO store_price (store_id, product_id, price)
    VALUES (2, 2, 10);
INSERT INTO store_price (store_id, product_id, price)
    VALUES (3, 3, 50);
INSERT INTO store_price (store_id, product_id, price)
    VALUES (4, 4, 2);
INSERT INTO store_price (store_id, product_id, price)
    VALUES (5, 5, 30);
INSERT INTO store_price (store_id, product_id, price)
    VALUES (6, 6, 10);
INSERT INTO store_price (store_id, product_id, price)
    VALUES (7, 7, 40);
INSERT INTO store_price (store_id, product_id, price)
    VALUES (8, 8, 50000);
INSERT INTO store_price (store_id, product_id, price)
    VALUES (9, 9, 856600);
INSERT INTO store_price (store_id, product_id, price)
    VALUES (10, 10, 50);
INSERT INTO store_price (store_id, product_id, price)
    VALUES (2, 11, 10);
INSERT INTO store_price (store_id, product_id, price)
    VALUES (7, 12, 40);
INSERT INTO store_price (store_id, product_id, price)
    VALUES (5, 3, 16);
INSERT INTO store_price (store_id, product_id, price)
    VALUES (7, 5, 45);
INSERT INTO store_price (store_id, product_id, price)
    VALUES (1, 13, 9.5);
INSERT INTO store_price (store_id, product_id, price)
    VALUES (2, 14, 13.5);
INSERT INTO store_price (store_id, product_id, price)
    VALUES (11, 15, 200);
INSERT INTO store_price (store_id, product_id, price)
    VALUES (4, 14, 15);
INSERT INTO store_price (store_id, product_id, price)
    VALUES (12, 18, 1000);
INSERT INTO store_price (store_id, product_id, price)
    VALUES (12, 19, 3);
    
--orders
INSERT INTO orders (order_date, quantity, price, store_id, product_id, email, address)
    VALUES (to_date('4/21/2023', 'MM/DD/YYYY'), 1, 970, 1, 1, 'msadeghi@dawsoncollege.qc.ca', 'dawson college, montreal, qeuebe, canada');
INSERT INTO orders (order_date, quantity, price, store_id, product_id, email, address)
    VALUES (to_date('10/23/2023', 'MM/DD/YYYY'), 2, 20, 2, 2, 'alex@gmail.com', '090 boul saint laurent, montreal, quebec, canada');
INSERT INTO orders (order_date, quantity, price, store_id, product_id, email, address)
    VALUES (to_date('10/1/2023', 'MM/DD/YYYY'), 3, 150, 3, 3, 'marting@yahoo.com', 'brossard, quebec, canada');
INSERT INTO orders (order_date, quantity, price, store_id, product_id, email, address)
    VALUES (to_date('10/23/2023', 'MM/DD/YYYY'), 1, 2, 4, 4, 'daneil@yahoo.com', '100 atwater street, toronto, canada');
INSERT INTO orders (order_date, quantity, price, store_id, product_id, email, address)
    VALUES (to_date('10/23/2023', 'MM/DD/YYYY'), 1, 30, 5, 5, 'alex@gmail.com', '090 boul saint laurent, montreal, quebec, canada');
INSERT INTO orders (order_date, quantity, price, store_id, product_id, email, address)
    VALUES (to_date('10/10/2023', 'MM/DD/YYYY'), 1, 10, 6, 6, 'marting@yahoo.com', 'brossard, quebec, canada');
INSERT INTO orders (order_date, quantity, price, store_id, product_id, email, address)
    VALUES (to_date('10/11/2023', 'MM/DD/YYYY'), 1, 40, 7, 7, 'msadeghi@dawsoncollege.qc.ca', 'dawson college, montreal, qeuebe, canada');
INSERT INTO orders (order_date, quantity, price, store_id, product_id, email, address)
    VALUES (to_date('10/10/2023', 'MM/DD/YYYY'), 1, 50000, 8, 8, 'bdoura@gmail.com', '100 Young street, toronto, canada');
INSERT INTO orders (order_date, quantity, price, store_id, product_id, email, address)
    VALUES (to_date('5/6/2020', 'MM/DD/YYYY'), 6, 60, 2, 11, 'johnson.a@gmail.com', 'Calgary, Alberta, Canada');
INSERT INTO orders (order_date, quantity, price, store_id, product_id, email, address)
    VALUES (to_date('9/12/2019', 'MM/DD/YYYY'), 3, 30, 6, 6, 'marting@yahoo.com', 'brossard, quebec, canada');
INSERT INTO orders (order_date, quantity, price, store_id, product_id, email, address)
    VALUES (to_date('10/11/2010', 'MM/DD/YYYY'), 1, 80, 7, 12, 'msadeghi@dawsoncollege.qc.ca', 'dawson college, montreal, qeuebe, canada');
INSERT INTO orders (order_date, quantity, price, store_id, product_id, email, address)
    VALUES (to_date('5/6/2022', 'MM/DD/YYYY'), 7, 70, 2, 11, 'msadeghi@dawsoncollege.qc.ca', 'dawson college, montreal, qeuebe, canada');
INSERT INTO orders (order_date, quantity, price, store_id, product_id, email, address)
    VALUES (to_date('10/7/2023', 'MM/DD/YYYY'), 2, 160, 7, 12, 'ms@gmail.com', '104 gill street, Toronto, Canada');
INSERT INTO orders (order_date, quantity, price, store_id, product_id, email, address)
    VALUES (to_date('8/10/2023', 'MM/DD/YYYY'), 1, 50000, 8, 8, 'abcd@yahoo.com', '105 Young street, toronto, canada');
INSERT INTO orders (order_date, quantity, price, store_id, product_id, email, address)
    VALUES (to_date('10/23/2023', 'MM/DD/YYYY'), 1, 50, 5, 3, 'abcd@yahoo.com', '090 boul saint laurent, montreal, quebec, canada');
INSERT INTO orders (order_date, quantity, price, store_id, product_id, email, address)
    VALUES (to_date('10/2/2023', 'MM/DD/YYYY'), 1, 30, 7, 5, 'alex@gmail.com', '090 boul saint laurent, montreal, quebec, canada');
INSERT INTO orders (order_date, quantity, price, store_id, product_id, email, address)
    VALUES (to_date('4/3/2019', 'MM/DD/YYYY'), 1, 9.5, 1, 13, 'm.li@gmail.com', '87 boul saint laurent, montreal, quebec, canada');
INSERT INTO orders (order_date, quantity, price, store_id, product_id, email, address)
    VALUES (to_date('12/29/2021', 'MM/DD/YYYY'), 3, 40.5, 2, 14, 'smith@hotmail.com', '76 boul decalthon, laval, quebec, canada');
INSERT INTO orders (order_date, quantity, price, store_id, product_id, email, address)
    VALUES (to_date('1/20/2020', 'MM/DD/YYYY'), 1, 200, 11, 15, 'g.noah@yahoo.com', '22222 happy street, Laval, quebec, canada');
INSERT INTO orders (order_date, quantity, price, store_id, product_id, email, address)
    VALUES (to_date('10/11/2022', 'MM/DD/YYYY'), 1, 40, 7, 7, 'msadeghi@dawsoncollege.qc.ca', 'dawson college, montreal, qeuebe, canada');
INSERT INTO orders (order_date, quantity, price, store_id, product_id, email, address)
    VALUES (to_date('12/29/2021', 'MM/DD/YYYY'), 3, 40.5, 4, 14, 'smith@hotmail.com', '76 boul decalthon, laval, quebec, canada');

--warehouse
INSERT INTO warehouse (name, address) VALUES('warehouse A', '100 rue William, saint laurent, Quebec, Canada');
INSERT INTO warehouse (name, address) VALUES('Warehouse B', '304 Rue Fran�ois-Perrault, Villera Saint-Michel, Montr�al, QC');
INSERT INTO warehouse (name, address) VALUES('Warehouse C', '86700 Weston Rd, Toronto, Canada');
INSERT INTO warehouse (name, address) VALUES('Warehouse D', '170  Sideroad, Quebec City, Canada');
INSERT INTO warehouse (name, address) VALUES('Warehouse E', '1231 Trudea road, Ottawa, Canada ');
INSERT INTO warehouse (name, address) VALUES('Warehouse F', '16  Whitlock Rd, Alberta, Canada');
    
--warehouse_stock
INSERT INTO warehouse_stock (warehouse_id, product_id, quantity) VALUES( 1, 1, 1000);
INSERT INTO warehouse_stock (warehouse_id, product_id, quantity) VALUES( 1, 7, 10);
INSERT INTO warehouse_stock (warehouse_id, product_id, quantity) VALUES( 1, 8, 6);
INSERT INTO warehouse_stock (warehouse_id, product_id, quantity) VALUES( 1, 14, 2132);
INSERT INTO warehouse_stock (warehouse_id, product_id, quantity) VALUES( 1, 16, 352222);
INSERT INTO warehouse_stock (warehouse_id, product_id, quantity) VALUES( 2, 2, 24980);
INSERT INTO warehouse_stock (warehouse_id, product_id, quantity) VALUES( 2, 10, 39484);
INSERT INTO warehouse_stock (warehouse_id, product_id, quantity) VALUES( 3, 3, 103);
INSERT INTO warehouse_stock (warehouse_id, product_id, quantity) VALUES( 3, 11, 43242);
INSERT INTO warehouse_stock (warehouse_id, product_id, quantity) VALUES( 4, 4, 35405);
INSERT INTO warehouse_stock (warehouse_id, product_id, quantity) VALUES( 4, 11, 6579);
INSERT INTO warehouse_stock (warehouse_id, product_id, quantity) VALUES( 4, 15, 123);
INSERT INTO warehouse_stock (warehouse_id, product_id, quantity) VALUES( 5, 5, 40);
INSERT INTO warehouse_stock (warehouse_id, product_id, quantity) VALUES( 5, 9, 1000);
INSERT INTO warehouse_stock (warehouse_id, product_id, quantity) VALUES( 5, 12, 98765);
INSERT INTO warehouse_stock (warehouse_id, product_id, quantity) VALUES( 5, 17, 4543);
INSERT INTO warehouse_stock (warehouse_id, product_id, quantity) VALUES( 6, 6, 450);
INSERT INTO warehouse_stock (warehouse_id, product_id, quantity) VALUES( 6, 10, 3532);
INSERT INTO warehouse_stock (warehouse_id, product_id, quantity) VALUES( 6, 13, 43523);

commit;
