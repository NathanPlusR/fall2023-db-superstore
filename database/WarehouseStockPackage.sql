create or replace package warehouse_stock_package as
    procedure add_warehouse_stock(warehouse_id number, product_id number, quantity number);
    procedure remove_warehouse_stock(warehouse_to_remove_id in number, product_to_remove_id number);
    procedure update_warehouse_stock(warehouse_id_to_update in number, product_id_to_update number, quantity_change in number);
end warehouse_stock_package;
/
create or replace package body warehouse_stock_package as
    -- Adds a quantity of a product to a warehouse and enables 'update_warehouse_stock' to work.
    -- @param warehouse_id - ID of a warehouse
    -- @param product_id - ID of a product
    -- @param quantity - quantity of the product to add to the warehouse
    procedure add_warehouse_stock(warehouse_id number, product_id number, quantity number) as
    begin
        insert into warehouse_stock values (warehouse_id, product_id, quantity);
    end;
    
    -- Removes the existing stock of a product inside a warehouse and disables 'update_warehouse_stock' from working.
    -- @param warehouse_to_remove_id - ID of a warehouse
    -- @param product_to_remove_id - ID of a product
    procedure remove_warehouse_stock(warehouse_to_remove_id in number, product_to_remove_id number) as
        warehouse_temp NUMBER;
        product_temp NUMBER;
    begin
        select warehouse_id, product_id
        into warehouse_temp, product_temp 
        from warehouse_stock
        where product_id = product_to_remove_id and warehouse_id = warehouse_to_remove_id;
        
        if (warehouse_temp IS NULL or product_temp is null) then
            raise no_data_found;
        end if;
        
        delete from warehouse_stock where (product_id = product_to_remove_id and warehouse_id = warehouse_to_remove_id);
    end;
    
    -- Updates the quantity of the stock in a warehouse by adding or subtracting the quantity chaneg entered.
    -- @param warehouse_id_to_update - ID of a warehouse
    -- @param product_id_to_update - ID of a product
    -- @param quantity_change - positive or negative number representing the change to the existing quanity in the warehouse.
    procedure update_warehouse_stock(warehouse_id_to_update in number, product_id_to_update number, quantity_change in number) as
        warehouse_temp NUMBER;
        product_temp NUMBER;
    begin
        select warehouse_id, product_id
        into warehouse_temp, product_temp 
        from warehouse_stock
        where product_id = product_id_to_update and warehouse_id = warehouse_id_to_update;
        
        if (warehouse_temp IS NULL or product_temp is null) then
            raise no_data_found;
        end if;
        
        update warehouse_stock set 
        quantity = (case when quantity + quantity_change < 0 then 0 else quantity + quantity_change end)
        where product_id = product_id_to_update and warehouse_id = warehouse_id_to_update;
    end;
end warehouse_stock_package;