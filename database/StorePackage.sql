create or replace package store_package as
    procedure add_store(name in varchar2);
    procedure remove_store(store_to_remove_id in number);
    procedure update_store(existing_store_id in number, newName in varchar2);
end store_package;
/
create or replace package body store_package as
    procedure add_store(name in varchar2) as
    begin 
        insert into store(name) values(name);
    end;
    procedure remove_store(store_to_remove_id in number)as
        temp number;
    begin 
        select store_id into temp from store where store_id = store_to_remove_id;
        if (temp IS NULL) then
            raise no_data_found;
        else
            delete from store where store_id = store_to_remove_id;
        end if;
    end;
    procedure update_store(existing_store_id in number, newName in varchar2) as
        temp number;
    begin
        select store_id into temp from store where store_id = existing_store_id;
        if (temp IS NULL) then
            raise no_data_found;
        end if;
        
        update store set
        name = newName
        where store_id = existing_store_id;
    end;
end store_package;