create or replace package review_package as
    PROCEDURE add_review (vreview IN review_object);
    PROCEDURE delete_review (r_id IN NUMBER);
    PROCEDURE update_description (r_id IN NUMBER, r_description IN VARCHAR2);
    PROCEDURE update_flags (r_id IN NUMBER, r_flag IN NUMBER);
    FUNCTION product_average_reviews(p_id NUMBER) RETURN NUMBER;
    TYPE review_cursor is ref cursor;
    FUNCTION show_flagged_reviews RETURN review_cursor;
    invalid_score EXCEPTION;
    invalid_entries EXCEPTION;
end review_package;
/
create or replace package body review_package as
   -- Adds a customer review of a product
   -- @param vreview - review object containing initialized parameters.
    PROCEDURE add_review (vreview IN review_object)
        as
        begin
            if (vreview.score IS NULL AND vreview.description IS NULL)
            then
                    raise invalid_entries;
            end if;
            INSERT INTO review (score, description, customer_id, product_id, flag)
                VALUES (vreview.score, vreview.description, vreview.customer_id, vreview.product_id, 0);
        end;
    
    -- Deletes a review
    -- @param r_id - ID of a review
    PROCEDURE delete_review (r_id IN NUMBER)
        as
            temp NUMBER;
        begin
            Select review_id into temp from review where review_id = r_id;
            if (temp IS NULL) then
                raise no_data_found;
            else
                delete from review where review_id = r_id;
            end if;
        end;
    
    -- Updates the description of a review
    -- @param r_id - ID of a review
    -- @param r_description - new description to replace the old description with in the review.
    PROCEDURE update_description (r_id IN NUMBER, r_description IN VARCHAR2)
        as
            temp NUMBER;
        begin
            Select review_id into temp from review where review_id = r_id;
            if (temp IS NULL) then
                raise no_data_found;
            else
                update review SET
                description = r_description
                where review_id = r_id;
            end if;            
        end;
    
    -- Updates the flag value of a review
    -- @param r_id - ID of a review
    -- @param r_flag - number of flags for a review
    PROCEDURE update_flags (r_id IN NUMBER, r_flag IN NUMBER)
        as
            temp NUMBER;
        begin
            Select review_id into temp from review where review_id = r_id;
            if (temp IS NULL) then
                raise no_data_found;
            else
                update review SET
                flag = r_flag
                where review_id = r_id;
            end if;            
        end;
       
    -- Calculates the average score from reviews made by customers regarding a certain product.
    -- @param p_id - ID of a product
    -- @return average score of a product
    FUNCTION product_average_reviews(p_id NUMBER) RETURN NUMBER is
    average NUMBER;
        begin
            select AVG(r.score) into average from review r where r.product_id = p_id;
            if (average IS NULL)
            then
                    average:=0;
            end if;
            return average;
        end;
        
    -- Retrieves all reviews that have been flagged.
    -- @return review_cursor - list of reviews
    FUNCTION show_flagged_reviews RETURN review_cursor is
    info review_cursor;
        begin
            open info for SELECT * FROM review r where r.flag > 1;
            return info;
        end;
    
end review_package;