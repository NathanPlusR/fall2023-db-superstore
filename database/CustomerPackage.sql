create or replace package customer_package as
    PROCEDURE add_customer (new_fname IN VARCHAR2, new_lname IN VARCHAR2, new_id OUT NUMBER);
    PROCEDURE delete_customer (c_id IN NUMBER);
    type customer_cursor is ref cursor;
    PROCEDURE displayCustomers(info out customer_cursor);
    PROCEDURE displayCustomerByID(c_id in NUMBER, info out customer_cursor);
    PROCEDURE displayFlaggedCustomers(info out customer_cursor);
end customer_package;
/
create or replace package body customer_package as
   -- Adds a new customer 
   -- @param new_fname - first name of the customer
   -- @param new_lname - last name of the customer
   -- @param new_id - ID used to refer to the customer
    PROCEDURE add_customer (new_fname IN VARCHAR2, new_lname IN VARCHAR2, new_id OUT NUMBER)
        as
        begin
            INSERT INTO customer (fname, lname)
                VALUES (new_fname, new_lname);
            SELECT MAX(customer_id) into new_id from customer;
        end;
    
    -- Deletes a customer
    -- @param c_id - ID of the customer to delete
    PROCEDURE delete_customer (c_id IN NUMBER)
        as
            temp NUMBER;
        begin
            Select customer_id into temp from customer where customer_id = c_id;
            if (temp IS NULL) then
                raise no_data_found;
            else
                delete from customer where customer_id = c_id;
            end if;
        end;
    
    -- Retrieves a customer 
    -- @param c_id - the ID of the customer
    -- @param customer_cursor (custom cursor) - list containing only one customer 
    PROCEDURE displayCustomerByID(c_id in NUMBER, info out customer_cursor)
        as
        begin
            open info for SELECT * FROM customer where customer_id like c_id;
        end;
        
    -- Retrieves all customers
    -- @param customer_cursor (custom cursor)- list containing all customers
    PROCEDURE displayCustomers(info out customer_cursor)
        as
        begin
            open info for SELECT * FROM customer;
        end;
    
    -- Retrieves customers whose reviews have been flagged more than once
    -- @param info (custom cursor) - list containing customers with more than 1 flagged review
    PROCEDURE displayFlaggedCustomers(info out customer_cursor)
        as
        begin
            open info for SELECT * FROM customer c
            INNER JOIN review r on c.customer_id = r.customer_id
            WHERE r.flag > 0;
        end;
end customer_package;
/