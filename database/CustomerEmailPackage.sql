create or replace package customer_email_package as
    PROCEDURE add_customer_email (new_email IN VARCHAR2, c_id IN NUMBER);
    type email_cursor is ref cursor;
    PROCEDURE display_customer_emails(c_id in NUMBER, info out email_cursor);
end customer_email_package;
/
create or replace package body customer_email_package as
   -- Adds a new email for a customer
   -- @param new_email - new email for the customer
   -- @param c_id - ID of the customer
    PROCEDURE add_customer_email (new_email IN VARCHAR2, c_id IN NUMBER)
        as
        begin
            INSERT INTO customer_email (email, customer_id)
                VALUES (new_email, c_id);
        end;
        
    -- Retrieves emails for a specific customer
    -- @param c_id - ID of customer
    -- @param info - list of emails
    PROCEDURE display_customer_emails(c_id in NUMBER, info out email_cursor)
        as
        begin
            open info for SELECT * FROM customer_email where customer_id = c_id;
        end;
end customer_email_package;
