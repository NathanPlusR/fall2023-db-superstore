-- Removes all the tables created by InitializeDatabase and InitializeAudit
DROP TABLE	CUSTOMER	cascade constraints;
DROP TABLE	CUSTOMER_ADDRESS	cascade constraints;
DROP TABLE	CUSTOMER_EMAIL	cascade constraints;
DROP TABLE	ORDERS	cascade constraints;
DROP TABLE	PRODUCT	cascade constraints;
DROP TABLE	STORE	cascade constraints;
DROP TABLE	STORE_CATEGORY	cascade constraints;
DROP TABLE	STORE_PRICE	cascade constraints;
DROP TABLE	REVIEW	cascade constraints;
DROP TABLE	WAREHOUSE	cascade constraints;
DROP TABLE	WAREHOUSE_STOCK	cascade constraints;
DROP TABLE  AUDITS;

-- Removes all packages needed to run the program
DROP PACKAGE customer_package;
DROP PACKAGE customer_email_package;
DROP PACKAGE customer_address_package;
DROP PACKAGE orders_package;
DROP PACKAGE product_package;
DROP PACKAGE store_package;
DROP PACKAGE store_category_package;
DROP PACKAGE store_price_package;
DROP PACKAGE review_package;
DROP PACKAGE warehouse_package;
DROP PACKAGE warehouse_stock_package;
DROP PACKAGE audit_package;

-- Removes all objects needed to run the program
DROP TYPE review_object;
DROP TYPE orders_object;
DROP TYPE product_object;
