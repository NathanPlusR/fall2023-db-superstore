-- Object used to represent the review table
create or replace type review_object as object( 
    review_id number(10),
    score number(1),
    description varchar2(1000),
    flag number(3),
    customer_id number(10),
    product_id number(10)
);
/
-- Object used to represent the orders table
create or replace type orders_object as object(
    order_id number(10),
    order_date date,
    quantity number(5),
    email varchar2(100),
    address varchar2(1000),
    price number(9,2),
    store_id number(10),
    product_id number(10)
);
-- Object used to represent the products table
/
create or replace type product_object as object(
    product_id number(10),
    name varchar2(100),
    category varchar2(100),
    price number(9,2)
);