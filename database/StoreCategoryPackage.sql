create or replace package store_category_package as
    procedure add_store_category(category in varchar2, store_id number);
    procedure remove_store_category(store_category_to_remove in varchar2, store_id_to_remove number);
end store_category_package;
/

create or replace package body store_category_package as
    procedure add_store_category(category in varchar2, store_id number) as
    begin
        insert into store_category values (category, store_id);
    end;
    procedure remove_store_category(store_category_to_remove in varchar2, store_id_to_remove number) as
        category_temp varchar2(50);
        store_id_temp number;
    begin
        select category, store_id into category_temp, store_id_temp from store_category where category = store_category_to_remove and store_id = store_id_to_remove;
                    
        if (category_temp IS NULL or store_id_temp is null) then               
            raise no_data_found;
        end if;
        
        delete from store_category where category = store_category_to_remove and store_id = store_id_to_remove; 
        
    end;
end store_category_package;
/