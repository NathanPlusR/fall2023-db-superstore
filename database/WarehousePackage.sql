create or replace package warehouse_package as
    procedure add_warehouse(name in varchar2, address in varchar2);
    procedure remove_warehouse(warehouse_to_remove_id in number);
    procedure update_warehouse(existing_warehouse_id in number, newName in varchar2, newAddress in varchar2);
    type warehouse_cursor is ref cursor;
    PROCEDURE get_all_warehouses(info out warehouse_cursor);
end warehouse_package;
/
create or replace package body warehouse_package as
    -- Adds a warehouse
    -- @param name - name of the warehouse
    -- @param address - address of the warehouse
    procedure add_warehouse(name in varchar2, address in varchar2) as
    begin
        insert into warehouse(name, address) values (name, address);
    end;
    
    -- Removes a warehouse
    -- @param warehouse_to_remove_id - ID of the warehouse
    procedure remove_warehouse(warehouse_to_remove_id in number) as
        temp NUMBER;
    begin
        Select warehouse_id into temp from warehouse where warehouse_id = warehouse_to_remove_id;
        if (temp IS NULL) then
            raise no_data_found;
        else
            delete from warehouse where warehouse_id = warehouse_to_remove_id;
        end if;
    end;

    -- Updates the information of the warehosue
    -- @param existing_warehouse_id - ID of a warehouse
    -- @param newName - new name of the warehosue
    -- @param newAddress - new address of the warehosue
    procedure update_warehouse(existing_warehouse_id in number, newName in varchar2, newAddress in varchar2) as
        temp number;
    begin
        Select warehouse_id into temp from warehouse where warehouse_id = existing_warehouse_id;
        if (temp IS NULL) then
            raise no_data_found;
        end if;
    
        update warehouse set 
        name = newName,
        address = newAddress
        where warehouse_id = existing_warehouse_id;
    end;
    
    -- Retrieves all the warehouses
    -- @param info (customer cursor) - list of warehouses
    PROCEDURE get_all_warehouses(info out warehouse_cursor)
    as
    begin
         open info for SELECT * FROM warehouse;
    end;
end warehouse_package;