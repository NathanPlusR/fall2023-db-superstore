--Create Audit Table
CREATE TABLE Audits(
    audit_id NUMBER(10) GENERATED AS IDENTITY CONSTRAINT audit_pk PRIMARY KEY,
    table_changed VARCHAR2(20) NOT NULL,
    date_changed DATE,
    action VARCHAR2(20)
);
/

-- Triggers Creation

-- Customer 
CREATE OR REPLACE TRIGGER audit_customer_trigger
BEFORE DELETE OR INSERT OR UPDATE
ON customer FOR EACH ROW
DECLARE
    actionType Audits.action%TYPE;
BEGIN
    IF UPDATING THEN
        actionType := 'UPDATE';
    ELSIF DELETING THEN
        actionType := 'DELETE';
    ELSIF INSERTING THEN
        actionType := 'INSERT';
    END IF;
    
    INSERT INTO Audits (table_changed, date_changed, action)
    VALUES ('CUSTOMER', SYSDATE, actionType);
END;

/

--Customer Address
CREATE OR REPLACE TRIGGER audit_customer_address_trigger
BEFORE DELETE OR INSERT OR UPDATE
ON customer_address FOR EACH ROW
DECLARE
    actionType Audits.action%TYPE;
BEGIN
    IF UPDATING THEN
        actionType := 'UPDATE';
    ELSIF DELETING THEN
        actionType := 'DELETE';
    ELSIF INSERTING THEN
        actionType := 'INSERT';
    END IF;
    
    INSERT INTO Audits (table_changed, date_changed, action)
    VALUES ('CUSTOMER_ADDRESS', SYSDATE, actionType);
END;

/

-- Customer Email
CREATE OR REPLACE TRIGGER audit_customer_email_trigger
BEFORE DELETE OR INSERT OR UPDATE
ON customer_email FOR EACH ROW
DECLARE
    actionType Audits.action%TYPE;
BEGIN
    IF UPDATING THEN
        actionType := 'UPDATE';
    ELSIF DELETING THEN
        actionType := 'DELETE';
    ELSIF INSERTING THEN
        actionType := 'INSERT';
    END IF;
    
    INSERT INTO Audits (table_changed, date_changed, action)
    VALUES ('CUSTOMER_EMAIL', SYSDATE, actionType);
END;

/

--Orders
CREATE OR REPLACE TRIGGER audit_orders_trigger
BEFORE DELETE OR INSERT OR UPDATE
ON orders FOR EACH ROW
DECLARE
    actionType Audits.action%TYPE;
BEGIN
    IF UPDATING THEN
        actionType := 'UPDATE';
    ELSIF DELETING THEN
        actionType := 'DELETE';
    ELSIF INSERTING THEN
        actionType := 'INSERT';
    END IF;
    
    INSERT INTO Audits (table_changed, date_changed, action)
    VALUES ('ORDERS', SYSDATE, actionType);
END;

/


-- Prodcts
CREATE OR REPLACE TRIGGER audit_product_trigger
BEFORE DELETE OR INSERT OR UPDATE
ON product FOR EACH ROW
DECLARE
    actionType Audits.action%TYPE;
BEGIN
    IF UPDATING THEN
        actionType := 'UPDATE';
    ELSIF DELETING THEN
        actionType := 'DELETE';
    ELSIF INSERTING THEN
        actionType := 'INSERT';
    END IF;
    
    INSERT INTO Audits (table_changed, date_changed, action)
    VALUES ('PRODUCT', SYSDATE, actionType);
END;

/

-- Reviews
CREATE OR REPLACE TRIGGER audit_review_trigger
BEFORE DELETE OR INSERT OR UPDATE
ON review FOR EACH ROW
DECLARE
    actionType Audits.action%TYPE;
BEGIN
    IF UPDATING THEN
        actionType := 'UPDATE';
    ELSIF DELETING THEN
        actionType := 'DELETE';
    ELSIF INSERTING THEN
        actionType := 'INSERT';
    END IF;
    
    INSERT INTO Audits (table_changed, date_changed, action)
    VALUES ('REVIEW', SYSDATE, actionType);
END;

/

-- Stores
CREATE OR REPLACE TRIGGER audit_store_trigger
BEFORE DELETE OR INSERT OR UPDATE
ON store FOR EACH ROW
DECLARE
    actionType Audits.action%TYPE;
BEGIN
    IF UPDATING THEN
        actionType := 'UPDATE';
    ELSIF DELETING THEN
        actionType := 'DELETE';
    ELSIF INSERTING THEN
        actionType := 'INSERT';
    END IF;
    
    INSERT INTO Audits (table_changed, date_changed, action)
    VALUES ('STORE', SYSDATE, actionType);
END;

/

-- Store Category
CREATE OR REPLACE TRIGGER audit_store_category_trigger
BEFORE DELETE OR INSERT OR UPDATE
ON store_category FOR EACH ROW
DECLARE
    actionType Audits.action%TYPE;
BEGIN
    IF UPDATING THEN
        actionType := 'UPDATE';
    ELSIF DELETING THEN
        actionType := 'DELETE';
    ELSIF INSERTING THEN
        actionType := 'INSERT';
    END IF;
    
    INSERT INTO Audits (table_changed, date_changed, action)
    VALUES ('STORE_CATEGORY', SYSDATE, actionType);
END;

/

-- Store Price
CREATE OR REPLACE TRIGGER audit_store_price_trigger
BEFORE DELETE OR INSERT OR UPDATE
ON store_price FOR EACH ROW
DECLARE
    actionType Audits.action%TYPE;
BEGIN
    IF UPDATING THEN
        actionType := 'UPDATE';
    ELSIF DELETING THEN
        actionType := 'DELETE';
    ELSIF INSERTING THEN
        actionType := 'INSERT';
    END IF;
    
    INSERT INTO Audits (table_changed, date_changed, action)
    VALUES ('STORE_PRICE', SYSDATE, actionType);
END;

/

-- Warehouse
CREATE OR REPLACE TRIGGER audit_warehouse_trigger
BEFORE DELETE OR INSERT OR UPDATE
ON warehouse FOR EACH ROW
DECLARE
    actionType Audits.action%TYPE;
BEGIN
    IF UPDATING THEN
        actionType := 'UPDATE';
    ELSIF DELETING THEN
        actionType := 'DELETE';
    ELSIF INSERTING THEN
        actionType := 'INSERT';
    END IF;
    
    INSERT INTO Audits (table_changed, date_changed, action)
    VALUES ('WAREHOUSE', SYSDATE, actionType);
END;

/

-- Warehouse Stock
CREATE OR REPLACE TRIGGER audit_warehouse_stock_trigger
BEFORE DELETE OR INSERT OR UPDATE
ON warehouse_stock FOR EACH ROW
DECLARE
    actionType Audits.action%TYPE;
BEGIN
    IF UPDATING THEN
        actionType := 'UPDATE';
    ELSIF DELETING THEN
        actionType := 'DELETE';
    ELSIF INSERTING THEN
        actionType := 'INSERT';
    END IF;
    
    INSERT INTO Audits (table_changed, date_changed, action)
    VALUES ('WAREHOUSE_STOCK', SYSDATE, actionType);
END;

/
--Audit Package Header
CREATE OR REPLACE PACKAGE audit_package AS
    type audit_cursor is ref cursor;
    PROCEDURE display_audit(info out audit_cursor);
END audit_package;

/
--Audit Package Body
CREATE OR REPLACE PACKAGE BODY audit_package AS
    PROCEDURE display_audit(info out audit_cursor)
        as
        begin
            open info for SELECT * FROM audits;
        end;
END audit_package;
/