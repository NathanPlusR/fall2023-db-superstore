create or replace package store_price_package as
    procedure add_store_price(store_id in number, product_id in number, price in number);
    procedure remove_store_price(store_id_to_remove in number, product_id_to_remove in number);
    procedure update_store_price(store_id_to_update in number, product_id_to_update in number, newPrice number);
end store_price_package;
/
create or replace package body store_price_package as
    procedure add_store_price(store_id in number, product_id in number, price in number) as
    begin
        insert into store_price values (store_id, product_id, price);
    end;
    procedure remove_store_price(store_id_to_remove in number, product_id_to_remove in number) as
        store_id_temp number;
        product_id_temp varchar2(50);
    begin
        select store_id, product_id into store_id_temp, product_id_temp from store_price where store_id = store_id_to_remove and product_id = product_id_to_remove;
                    
        if (product_id_temp IS NULL or store_id_temp is null) then               
            raise no_data_found;
        end if;
        
        delete from store_price where store_id = store_id_to_remove and product_id = product_id_to_remove;
    end;
    procedure update_store_price(store_id_to_update in number, product_id_to_update in number, newPrice number) as
        store_id_temp number;
        product_id_temp varchar2(50);
    begin
        select store_id, product_id into store_id_temp, product_id_temp from store_price where store_id = store_id_to_update and product_id = product_id_to_update;
                    
        if (product_id_temp IS NULL or store_id_temp is null) then               
            raise no_data_found;
        end if;
        
        update store_price set 
        price = newPrice
        where store_id = store_id_to_update 
        and product_id = product_id_to_update;
    end;
end store_price_package;