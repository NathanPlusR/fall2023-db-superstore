create or replace package product_package as
    PROCEDURE add_product (vproduct IN product_object);
    PROCEDURE delete_product (p_id IN NUMBER);
    PROCEDURE update_product (vproduct IN product_object);
    type product_cursor is ref cursor;
    PROCEDURE display_products(requested_category in VARCHAR2, info out product_cursor);
    PROCEDURE display_products_stock(p_id IN NUMBER, info out product_cursor);
    PROCEDURE count_ordered(p_id IN NUMBER, count out NUMBER);
    invalid_price EXCEPTION;
end product_package;
/
create or replace package body product_package as
   -- Adds a new product
   -- @param vproduct - product object containing initialized parameters
    PROCEDURE add_product (vproduct IN product_object)
        as
        begin
            if (vproduct.price < 0)
            then
                    raise invalid_price;
            end if;
            INSERT INTO product (name, category, price)
                VALUES (vproduct.name, vproduct.category, vproduct.price);
        end;
    
    -- Removes a product
    -- @param p_id - ID of product 
    PROCEDURE delete_product (p_id IN NUMBER)
        as
            temp NUMBER;
        begin
            Select product_id into temp from product where product_id = p_id;
            if (temp IS NULL) then
                raise no_data_found;
            else
                delete from product where product_id = p_id;
            end if;
        end;
        
    -- Updates a product's information
    -- @param vproduct - product object containing existing ID and initialized parameters
    PROCEDURE update_product (vproduct IN product_object)
        as
            temp NUMBER;
        begin
            Select product_id into temp from product where product_id = vproduct.product_id;
            if (temp IS NULL) then
                raise no_data_found;
            else
                if (vproduct.price < 0)
                then
                        raise invalid_price;
                end if;
                update product SET
                name = vproduct.name, 
                category = vproduct.category, 
                price = vproduct.price
                where product_id = vproduct.product_id;
            end if;            
        end;
        
    -- Retrieves all products from a specific category or all categories
    -- @param requested_category - name of existing category. If input is '%' all products are retrieved.
    -- @param info (custom cursor) - list of products 
    PROCEDURE display_products(requested_category in VARCHAR2, info out product_cursor)
        as
        begin
            open info for SELECT * FROM product where category like requested_category;
        end;
        
    PROCEDURE display_products_stock(p_id in NUMBER, info out product_cursor)
        as
        begin
            open info for select SUM(ws.quantity)from product p
                inner join warehouse_stock ws on p.product_id = ws.product_id
                where p.product_id = p_id
                group by p.product_id;
        end;
        
    -- Counts the number of times a product has been ordered. Does not count the quantity purchased in each order.
    -- @param p_id - ID of a product
    -- @param count - number of times the product was ordered
    PROCEDURE count_ordered(p_id IN NUMBER, count out NUMBER)
        as
        begin
            SELECT COUNT(o.product_id) into count from product p
            inner join orders o ON o.product_id = p.product_id
            where p.product_id = p_id;
        end;
end product_package;