create or replace package customer_address_package as
    PROCEDURE add_customer_address (new_address IN VARCHAR2, c_id IN NUMBER);
    type address_cursor is ref cursor;
    PROCEDURE display_customer_addresses(c_id in NUMBER, info out address_cursor);
end customer_address_package;
/
create or replace package body customer_address_package as
   -- Adds a new address for a customer
   -- @param new_address - The exact address of the customer
   -- @param c_id - ID of the customer
    PROCEDURE add_customer_address (new_address IN VARCHAR2, c_id IN NUMBER)
        as
        begin
            INSERT INTO customer_address (address, customer_id)
                VALUES (new_address, c_id);
        end;
    
    -- Retrieves all the addresses for a specific customer
    -- @param c_id - ID of the customer 
    -- @param address_cursor (custome cursor) - list of addresses
    PROCEDURE display_customer_addresses(c_id in NUMBER, info out address_cursor)
        as
        begin
            open info for SELECT * FROM customer_address where customer_id = c_id;
        end;
end customer_address_package;
/