# Fall2023 DB Superstore

# Authors: 
    Mohamed Aljak 2237281
    Nathan Bokobza 2236219


# Repository:
    https://gitlab.com/NathanPlusR/fall2023-db-superstore


# Install:

To install the database's tables, use setup.sql to create all the tables and insert the data into them.

After that, run InitializeAudit and InitializeObjects to add the Audit table with triggers and the custom types the DB uses.
Following that, run all the package files, denoted by ______Package.sql.

Once all the files in the /database folder are ran, the database will be ready for usage by the application!

The Application.java file can be found in the project\src\main\java\dawson folder relative to the path of this file. Once compiled
and ran, the DB will be ready for user access.


# Table design:

The customer table contains customers. It feilds include the unique identifier being the customer_id primary key and fname & lname 
which represents the customer's first and last names respectively.

The customer_email table contains the primary key of email and refferences the customers table by using customer_id as a foreign key. 
Email is a unique primary key one customer can have several emails, but one email can't be shared by several customers as in our 
interpretations, this functiond like online accounts where the user would need to log into an account with their email.

The customer_address table contains the addresses where customers live. It refferences the customer table using customer_id as a foreign 
key, and it's primary key is the address where the customer wants their product delivered.

The product table stores the different products sold. It has a product_id field which acts as a unique primary key, and it additionally 
has a name, category, and price field.

The reviews table contains the reviews for the individual products. Customers who bought products can leave reviews there. These reviews 
can have a score from 1-5 depending on how good the product was, a description describing the faults or benefits of the product, or both. 
Additionally it hosts a field called flag. The flag field contains the amount of times a review has been flagged for being inapropriate, 
and a check was implemented to make sure  review cannot have a negative number of flags. The table also features a primary key for the JDBC 
application to identify it in review_id. It additionally refferences the customer and product table via customer_id and product_id respectfully.

The warehouse table contains the individual warehouses providing to the SuperStore. Those warehouses are identified with a warehouse_id 
primary key, and the other two fields are the warehouse's name, which is unique, and the address of the warehouse.

The warehouse_stock table contains a composite key composed of the product_id and the warehouse_id of the product being stored and where 
it is being stored. The warehouse_stock table's goal is to track the quantity of a given product in a given warehouse, which is why it's 
third field is the quantity field. This field additionally has a check making sure the quantity can never dip below zero.

The store table contains the stores which are supplied to by the superstore. The table contains the unique identifier of store_id as a primary
key, and it's other field, name, is to contain the name of the store.

The store_category table is a many to many relationship, and therefore features a composite key of store_id and category. The idea is that one
store can have many categories, and many stores can have the same category. It uses store_id as a foreign key to refference the Store table.

The store_price table contains another composite key, being composed of foreign keys store_id and product_id whcih lead back to the store and
product tables respectively. The third entry of the table is price. This entry can't be made null as the draw of the table is retrieving the
specific price of a product in a specific store. Additionally due to logic reasons, the price cannot be less than zero and this is ensured by
a check.

Last but evidently not least is the orders table. The orders table contains the orders made by the customers. It contains a unique identifier
being order_id. Order_date is the date which the product was orderded, quantity is the quantity of the product ordered. Email and Address are
fields representing the customer's email and address. Customer_id is excluded here, as it can be retreived later throught the email as emails
can only have once customer and we wanted to avoid partial dependencies. If customer_id was placed in the table instead of email and address,
then it would be impossible to pin down where to ship the order, or how to get in touch with the customer, as one customer can have several
emails and several shippinh addresses. Price is the total cost of the order, and it features a check to make sure it doesn't go below zero.
Store_id and product id are also used to refference the product being bought and where it was being bought from by being foreign keys to the
product and store tables respectively.


# Additional things to note:

We created the store packages for general functionality even if they never get called. We were worried deleting them would make the packages
in SQL look incomplete however, and we were unwilling to take a risk like that that could backfire for us especially when we had already done the work.

Our store in Java is the BettyAlley Disk Store. For it we added a store called BettyAlley, gave it the store_category of Disk, and gave it two
custom products being the disks for "Amour Plastique" and "Monty Python".


Thank you for using our database :D
-Nathan & Mohamed